package Stage2.simulator

import Stage2.graphLib.topology.DGraph

import scala.collection.mutable.ArrayBuffer

object EventFunctions{
	def linkDown(link:String)(graph:DGraph):Unit={
		val edges  = findIdsFromString(link, graph)
		graph.downEdges = graph.downEdges ++ edges
	}
	def linkUp(link:String)(graph:DGraph):Unit={
		val edges  = findIdsFromString(link, graph)
		graph.downEdges = graph.downEdges -- edges
	}
	def linksDown(links:Seq[String])(graph:DGraph):Unit={
		for (link <- links)
			linkDown(link)(graph)
	}
	def linksUp(links:Seq[String])(graph:DGraph):Unit={
		for (link <- links)
			linkUp(link)(graph)
	}
	def findIdsFromString(link:String, graph:DGraph):ArrayBuffer[Int]={
		val nodesNames = link.split("-")
		val nodes = nodesNames map(name => graph.getNodeId(name))
		val edges = ArrayBuffer[Int]()
		for (i <- 0 until nodes.length-1)
			edges+=graph.getEdgeId((nodes(i), nodes(i+1)))
		edges
	}
	def nodeDown(node:String)(graph:DGraph):Unit={
		val nodeID = graph.getNodeId(node)
		val edges = graph.adjEdges(nodeID) map(e => graph.getEdgeId((e.src.id, e.dest.id)))
		graph.downEdges = graph.downEdges ++ edges
	}

	def nodeUp(node:String)(graph:DGraph):Unit={
		val nodeID = graph.getNodeId(node)
		val edges = graph.adjEdges(nodeID) map(e => graph.getEdgeId((e.src.id, e.dest.id)))
		graph.downEdges = graph.downEdges -- edges
	}
	def nodesDown(nodes:Seq[String])(graph:DGraph):Unit={
		for (node <- nodes)
			nodeDown(node)(graph)
	}
	def nodesUp(nodes:Seq[String])(graph:DGraph):Unit={
		for (node <- nodes)
			nodeUp(node)(graph)
	}
}