package Stage2.simulator

import Stage2.graphLib.topology.DGraph
import Stage2.misc.Path

trait PreEvent extends Event{
	val in: DGraph => Unit
	val out: DGraph => Unit
	def apply(graph : DGraph):Unit={
		in(graph)
	}
	def revert(graph : DGraph):Unit={
		out(graph)
	}
}

trait PostEvent extends Event{
	val postTreatment: Array[Array[Array[Path]]] => Array[Array[Array[Path]]]
	def apply(paths:Array[Array[Array[Path]]]):Array[Array[Array[Path]]]={
		postTreatment(paths)
	}
}