package Stage2.simulator

import Stage2.graphLib.topology.DGraph

trait Event{ 

	val duration:Int

	var on:Boolean = false
	var startTime:Int = -1
	var totalTime:Int = -1
	var graph:DGraph = null
	def dgraph = {graph}
	var stateString:String = ""

	//TO BE DEFINED
	// def apply(graph : DGraph):Unit
	// def revert(graph : DGraph):Unit
	def startCondition(time:Int):Boolean

	override def toString():String={
		stateString
	// def
	}

	def setTotalTime(sampleSize:Int):Unit={
		totalTime = sampleSize
	}

	def setGraph(newGraph:DGraph):Unit={
		graph = newGraph
		// println("DONE THAT")
		// if (graph == null)
		// 	println("GRAPH NULL")
	}

	def isOn(time:Int):Boolean={
		//TIME TO START
		val start = startCondition(time)
		if(start && !on){
			on = true
			startTime = time
		//TIME TO END
		}else if (on && startTime+duration <= time){
			//CHANGE MADE HERE INDEED. IF THE RANDOM MAKE IT START AGAIN, SHOULD IT NOT START ? then just restart the starttime
			//on = false
			if(start)
				startTime = time
			else
				on = false
		}
		on
	}
}