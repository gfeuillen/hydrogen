package Stage2.solver

import Stage2.graphLib.topology.DGraph
import Stage2.misc.Path

object Solver{
	def solve(solver_type:Int,pathsMatrix: Array[Array[Array[Path]]], topology: DGraph, pathNumber:Int):(Array[Double], Array[Double])={
		if (solver_type == 1)
			InverseShortestPaths.optimize(pathsMatrix, topology, pathNumber)			
		else //if (solver_type == 2)
			InverseShortestPaths_control.optimize(pathsMatrix, topology, pathNumber)
	}
}