package Stage2.tests

import java.util.Calendar
import java.io._

import scala.collection.mutable.ArrayBuffer
import java.nio.file.{Files, Paths}
import java.nio.file.{Files, Paths}

import Stage2.graphLib.topology.DGraph
import Stage2.misc.Path
import Stage2.simulator.{Event, Simulator}
import Stage2.solver.Solver
import Stage2.utils.{DataPreProcessors, DataUtils, GraphAnalyser}


class New_Test(
	//Parameters relative to the test
	val test_name:String,

	//Inputs
	val input_graph:DGraph,

	//Outputs
	val real_traces:String,
	val out_traces:String,

	//Parameters to store intermediate results
	val input_traces:String,
	val preprocessed_traces:String,

	//Parameters relative to hypothesis
	val sub_optimal:Boolean,
	val reverse_optimal:Boolean,

	//Parameters relative to solver
	val solver_type:Int, //If 1 use paper solver, if 2 use our solver
	val weights_file:String,
	val errors_file:String,	

	//Parameters relative to the simulator
	val log_file:String,
	val sampling_size:Int,
	val events:Seq[Event]
	){

	var metaData = "### "+Calendar.getInstance().getTime().toString()+":"+test_name+"\n"
	metaData += "### "+"Hypothesis : \n"
	metaData += "### "+"        Sub optimality : "+sub_optimal+"\n"
	metaData += "### "+"        Reverse optimality : "+reverse_optimal+"\n"
	metaData += "### "+s"Using solver type $solver_type \n"

	def perform(){
		//Create and launch the simulator
		val simulator = new Simulator(input_graph,log_file,input_traces,sampling_size)
		for (e <- events)
			simulator.addEvent(e)
		simulator.simulate()

		//PreProcess the traces
		DataPreProcessors.preProcess(sub_optimal, reverse_optimal)(input_traces,preprocessed_traces)

		//Launch the solver
		val (input_matrix,pathNumber) = DataUtils.loadPathMatrix(preprocessed_traces,input_graph)

		//Resolve the weights and shortest path by optimization
		val (weights, errors) = Solver.solve(solver_type,input_matrix, input_graph, pathNumber)

		//Write the weights
		val weights_writer = new FileWriter(new File(weights_file))
		weights_writer.write(metaData)
		for(e <- input_graph.edges)
			weights_writer.write(e+" : "+weights(e.id)+"\n")
		weights_writer.close()

		//Write the errors
		val errors_writer = new FileWriter(new File(errors_file))
		errors_writer.write(metaData)
		for(e <- errors)
			errors_writer.write(e+"\n")
		errors_writer.close()

		//Check if real traces have already been computed (quicker to load than compute)
		val done = Files.exists(Paths.get(real_traces))

		var reals = Array[Array[Array[Path]]]()
		var realsNumber = -1

		if(! done){
			//Compute  and write the real matrix
			val (reals1, realsNumber1) = GraphAnalyser.shortestPaths(input_graph, e => e.weight) 
			reals = reals1
			realsNumber = realsNumber1
			DataUtils.writePaths(reals,input_graph,real_traces)
		}
		else{
			//load it 
			val (reals1, realsNumber1)= DataUtils.loadPathMatrix(real_traces,input_graph)
			reals = reals1
			realsNumber = realsNumber1
		}
		

		val origin_weights = ArrayBuffer[Double]()
		//Compute and write predictions matrix

		//First assign the new weights
		for(i <- 0 until weights.size){
			val edge = input_graph.edge(i)
			origin_weights += edge.weight
			edge.weight = weights(i)
		}
		//Get edges part of the traceroute
		var inTraceEdges = Set[Int]()
		for (l <- input_matrix; c <- l; tr <- c; e <- tr.edges){
			inTraceEdges = inTraceEdges + e
		}

		//Remove edges that were not part of the traces
		for(e <- input_graph.edges){
			if(! inTraceEdges.contains(e.id))
				input_graph.downEdges += e.id
		}

		val (pred, predNumber) = GraphAnalyser.shortestPaths(input_graph, e => e.weight) 

		//Reset the down edges
		input_graph.downEdges = ArrayBuffer[Int]()
		//Reset the weights
		for(i <- 0 until weights.size){
			val edge = input_graph.edge(i)
			edge.weight = origin_weights(i)
		}

		DataUtils.writePaths(pred,input_graph,out_traces)
	}
}