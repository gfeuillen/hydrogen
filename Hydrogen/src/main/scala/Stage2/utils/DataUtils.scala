package Stage2.utils

import scala.io.Source
import java.io.File
import java.io.PrintWriter
import java.nio.file.{Files, Paths}

import Stage2.graphLib.topology.DGraph
import Stage2.misc.Path

import scala.collection.mutable.ArrayBuffer

object DataUtils {
	def writePaths(paths:Array[Array[Array[Path]]], graph:DGraph, fileName:String):Unit={
		val out = new File(fileName)

		if (out.exists())
			out.delete()

		val writer = new PrintWriter(out)
		for(l <- paths; c <- l; p <- c){
			val tmp = ArrayBuffer[String]()
			var id = graph.edges(p.edges(0)).src.id
			tmp.append(graph.nodes(id).label)
			for(e <- p.edges){
				id = graph.edges(e).dest.id
				tmp.append(graph.nodes(id).label)
			}
			//tmp.append(p.dest)
			writer.write(tmp.mkString(" ")+"\n")
		}
		writer.close()
	}
	def copyPathMatrix(paths:Array[Array[Array[Path]]]):Array[Array[Array[Path]]]={
		return paths.clone()
	}
	def loadPathMatrix(preProcessedTracesFile:String, graph:DGraph):(Array[Array[Array[Path]]],Int)={
		val (paths, pathNumber) = Parser.parseTraces(preProcessedTracesFile, graph)

		//Create the path matrix
		val pathsMatrix = Array.fill(graph.nNodes, graph.nNodes)(Array[Path]())
		for (path <- paths){
			pathsMatrix(path.src)(path.dest) = pathsMatrix(path.src)(path.dest) :+ path
		}
		(pathsMatrix,pathNumber)
	}
}