package Stage2.graphLib.topology

import scala.collection.mutable.Map
import scala.collection.mutable.ArrayBuffer

class Node(val id: Int, val label: String, val x:Float, val y:Float) {
  override val toString: String = label
}

class Edge(val id: Int, val src: Node, val dest: Node, var weight:Double, val bandwidth:Int, val delay:Int) {
  override val toString: String = s"$src -> $dest"
}


//Edge : src, dst, weight, bw, delay
class DGraph(nodeNames: Seq[(String,Float,Float)], var capacitedEdges: Seq[(Int, Int, Double, Int, Int)], nodeToId: Map[String, Int], edgeToId: Map[(Int, Int), Int]) {

  //Not use of a set, repetition is good here, since the link should be up for every one for it to be up
  //Not sure if that's easy to understand
  var downEdges = ArrayBuffer[Int]()

  val nNodes = nodeNames.size
  val nEdges = capacitedEdges.size
  
  val Nodes = 0 until nNodes
  val Edges = 0 until nEdges
  
  // Ensures a fast access to nodes
  private val _nodes: Array[Node] = nodeNames.zipWithIndex.map {
    case ((name, x, y), id) => {
      new Node(id, name, x, y)
    }
  }.toArray
  
  // Ensures a fast access to edges
  private val _edges: Array[Edge] = capacitedEdges.zipWithIndex.map {
    case ((src, dest, w, bw, d), id) => {
      new Edge(id, _nodes(src), _nodes(dest), w, bw, d)
    }
  }.toArray

  // Ensures a fast access to adjacent edges
  private val _outEdges: Array[IndexedSeq[Int]] = Array.tabulate(nNodes)(n => Edges.filter(e => _edges(e).src == _nodes(n))) 
  private val _inEdges: Array[IndexedSeq[Int]] = Array.tabulate(nNodes)(n => Edges.filter(e => _edges(e).dest == _nodes(n)))
  private val _adjEdges: Array[IndexedSeq[Int]] = Array.tabulate(nNodes)(n => _inEdges(n) ++ _outEdges(n))
  
  // Ensures a fast access to adjacent nodes
  private val _outNodes: Array[IndexedSeq[Int]] = _outEdges.map(_.map(_edges(_).dest.id))
  private val _inNodes: Array[IndexedSeq[Int]] = _inEdges.map(_.map(_edges(_).src.id))
  private val _adjNodes: Array[IndexedSeq[Int]] = Array.tabulate(nNodes)(n => _inNodes(n) ++ _outNodes(n))

  def edges: IndexedSeq[Edge] = _edges.filter(e => !downEdges.contains(e.id))
  def allEdges: IndexedSeq[Edge] = _edges
  def nodes: IndexedSeq[Node] = _nodes

  def edge(edgeId: Int): Edge = _edges(edgeId)
  def node(nodeId: Int): Node = _nodes(nodeId)
  
  def outEdges(node: Node): Seq[Edge] = outEdges(node.id)
  def inEdges(node: Node): Seq[Edge] = inEdges(node.id)
  def adjEdges(node: Node): Seq[Edge] = adjEdges(node.id)
  
  def outNodes(node: Node): Seq[Node] = outNodes(node.id)
  def inNodes(node: Node): Seq[Node] = inNodes(node.id)
  def adjNodes(node: Node): Seq[Node] = adjNodes(node.id)
  
  def outEdges(nodeId: Int): Seq[Edge] = _outEdges(nodeId).map(_edges(_)).filter(e => !downEdges.contains(e.id))
  def inEdges(nodeId: Int): Seq[Edge] = _inEdges(nodeId).map(_edges(_))
  def adjEdges(nodeId: Int): Seq[Edge] = _adjEdges(nodeId).map(_edges(_))
  
  def outNodes(nodeId: Int): Seq[Node] = _outNodes(nodeId).map(_nodes(_))
  def inNodes(nodeId: Int): Seq[Node] = _inNodes(nodeId).map(_nodes(_))
  def adjNodes(nodeId: Int): Seq[Node] = _adjNodes(nodeId).map(_nodes(_))
  
  def outEdgesId(node: Node): Seq[Int] = _outEdges(node.id)
  def inEdgesId(node: Node): Seq[Int] = _inEdges(node.id)
  def adjEdgesId(node: Node): Seq[Int] = _adjEdges(node.id)
  
  def outNodesId(node: Node): Seq[Int] = _outNodes(node.id)
  def inNodesId(node: Node): Seq[Int] = _inNodes(node.id)
  def adjNodesId(node: Node): Seq[Int] = _adjNodes(node.id)
  
  def outEdgesId(nodeId: Int): Seq[Int] = _outEdges(nodeId)
  def inEdgesId(nodeId: Int): Seq[Int] = _inEdges(nodeId)
  def adjEdgesId(nodeId: Int): Seq[Int] = _adjEdges(nodeId)
  
  def outNodesId(nodeId: Int): Seq[Int] = _outNodes(nodeId)
  def inNodesId(nodeId: Int): Seq[Int] = _inNodes(nodeId)
  def adjNodesId(nodeId: Int): Seq[Int] = _adjNodes(nodeId)

  def getNodeId(label: String): Int = nodeToId(label)
  def getEdgeId(edge:(Int,Int)): Int = edgeToId(edge)
}