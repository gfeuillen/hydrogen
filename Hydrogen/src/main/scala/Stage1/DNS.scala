package Stage1

import java.io.File

import scala.collection.mutable
import scala.io.Source

/**
  * Created by maximedemol on 16/06/16.
  */
object DNS {

  def dnsNames(in: String) : mutable.HashMap[String, String] = {
    val file = new File(in)
    val map = new mutable.HashMap[String, String]
    for(f <- file.listFiles() if !f.getName.startsWith(".")){
      Source.fromFile(f).getLines().toVector.foreach(l => {
        val s = l.split("\\s+")
        var addr = ""
        var dns = ""
        try {
          addr = s(1)
          dns = s(2)
        } catch {
          case e: ArrayIndexOutOfBoundsException => dns = "FAIL.NOT_FOUND"
        }
        if(!map.contains(addr)){
          map(addr) = dns
        } else {
          if(map(addr).contains("FAIL.") && !dns.contains("FAIL.")) map(addr) = dns
        }
      })
    }
    map
  }

}
