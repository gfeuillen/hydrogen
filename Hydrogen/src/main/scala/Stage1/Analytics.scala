package Stage1

import java.io.{BufferedWriter, File, FileWriter}

import com.googlecode.ipv6.{IPv6Address, IPv6NetworkMask}

import scala.collection.mutable
import scala.io.Source
import scala.sys.process._

/**
  * Created by maximedemol on 29/05/16.
  */
object Analytics {

  def ipCount(in : String, out: String) : Unit = {
    val map = new mutable.HashMap[String, Int]()
    var traces = 0
    val file = new File(in)
    val fs = file.listFiles().length
    var cntF = 0
    for(w <- file.listFiles() if !w.getName.startsWith(".")) {
      val tmp = new File("tmp")
      tmp.createNewFile()
      ("sc_analysis_dump " + w) #> tmp !;
      Source.fromFile(tmp).getLines().toVector.foreach(l => {
        if (!l.startsWith("#") && l != "") {
          val str = if (l.count(_ == "T ") > 1) l.substring(0, l.indexOf("T ", 2)) else l
          val route = CaidaTraceroute.deserialize(str)
          traces+=1
          if (map.contains(route.source)) map(route.source) += 1 else map(route.source) = 1
          if (map.contains(route.destination)) map(route.destination) += 1 else map(route.source) = 1
          //println(route)
          for (h <- route.hops if !(h == null) && !h.qVal) {
            if (map.contains(h.IP)) map(h.IP) += 1 else map(h.IP) = 1
          }
        }
      })
      cntF += 1
      println("Filtered file "+cntF+" of "+fs)
    }
    var sum = 0
    map.values.foreach(sum += _)
    println("#Traceroute = " + traces)
    println("sum IP = " + sum)
    println("#IP = " + map.size)
    val fileOut = new File(out)
    fileOut.createNewFile()
    val bw = new BufferedWriter(new FileWriter(new File(out)))
    for (ip <- map){
      bw.write(ip._1 + " " + ip._2+"\n")
    }
    bw.close()
  }

  def ispCount(in: String, out: String) : Unit = {
    val map = new mutable.HashMap[String, Int]()
    val set = new mutable.HashSet[String]()
    val file = new File(in)
    val fs = file.listFiles().length
    var cntF = 0
    for(w <- file.listFiles() if !w.getName.startsWith(".")) {
      val tmp = new File("tmp")
      tmp.createNewFile()
      ("sc_analysis_dump " + w) #> tmp !;
      Source.fromFile(tmp).getLines().toVector.foreach(l => {
        if (!l.startsWith("#") && l != "") {
          val str = if (l.count(_ == "T ") > 1) l.substring(0, l.indexOf("T ", 2)) else l
          val route = CaidaTraceroute.deserialize(str)
          val src = IPv6Address.fromString(route.source).maskWithNetworkMask(IPv6NetworkMask.fromPrefixLength(32)).toString
          val dst = IPv6Address.fromString(route.destination).maskWithNetworkMask(IPv6NetworkMask.fromPrefixLength(32)).toString
          if (map.contains(src) && !set.contains(src)) {
            map(src) += 1
            set += src
          } else if (!set.contains(src)){
            map(src) = 1
            set += src
          }
          if (map.contains(dst) && !set.contains(dst)) {
            map(dst) += 1
            set += dst
          } else if (!set.contains(dst)) {
            map(dst) = 1
            set += dst
          }
          //println(route)
          for (h <- route.hops if !(h == null) && !h.qVal) {
            val pre = IPv6Address.fromString(h.IP).maskWithNetworkMask(IPv6NetworkMask.fromPrefixLength(32)).toString
            if (map.contains(pre) && !set.contains(pre)) {
              map(pre) += 1
              set += pre
            } else if (!set.contains(pre)){
              map(pre) = 1
              set += pre
            }
          }
        }
        set.clear()
      })
      cntF += 1
      println("Filtered file "+cntF+" of "+fs)
    }
    val fileOut = new File(out)
    fileOut.createNewFile()
    val bw = new BufferedWriter(new FileWriter(new File(out)))
    for (ip <- map){
      bw.write(ip._1 + " " + ip._2+"\n")
    }
    bw.close()
  }
}
