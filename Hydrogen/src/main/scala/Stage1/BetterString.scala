package Stage1

/**
  * Created by maximedemol on 12/05/16.
  */
class BetterString(s: String) {

  implicit def containsL(words: Array[String]) : Boolean = {
    for (word <- words){
      if (s.contains(word)) return true
    }
    false
  }

}

object BetterString {

  implicit def stringToString(s: String) : BetterString = new BetterString(s)

}