package Stage1

import java.io.File
import java.net.{InetAddress, UnknownHostException}

import org.xbill.DNS.Address

import scala.collection.mutable
import scala.io.Source

import Stage1.BetterString._

/**
  * Created by maximedemol on 16/06/16.
  */
object Inference {

  def completeRouters(trace: CaidaTraceroute, prefix: Array[String], dns: String, name: String, prev: (Int, mutable.HashMap[(String, String), Int])) : (Int, mutable.HashMap[(String, String), Int]) = {
    var (counter, map) = prev
    val hops = trace.hops
    val l = hops.length
    for (i <- 2 until l){
      if(!hops(i).qVal && !hops(i-1).qVal && !hops(i-2).qVal && hops(i).DNS.contains(dns) && hops(i-2).DNS.contains(dns) && hops(i-1).IP.containsL(prefix) && hops(i-1).DNS.contains("FAIL.")){
        var ns = ""
        try {
          ns = Address.getHostName(InetAddress.getByName(hops(i - 1).IP.dropRight(1)+"1"))
          //ns = if (ns.contains(dns)) ns else Address.getHostName(InetAddress.getByName(hops(i - 1).IP(0).dropRight(1)+"2"))
        } catch {
          case e: UnknownHostException => ns = hops(i-1).IP
        }
        try {
          ns = if (ns.contains(dns)) ns else Address.getHostName(InetAddress.getByName(hops(i - 1).IP.dropRight(1)+"2"))
        } catch {
          case e: UnknownHostException => ns = hops(i-1).IP
        }
        ns = if (ns.takeRight(1)==".") ns.dropRight(1) else ns
        hops(i-1).DNS = if (ns.contains(dns)) ns else {
          if(map.contains((hops(i-1).IP,""))) {name + map((hops(i-1).IP,""))}
          else {
            map((hops(i-1).IP,"")) = counter
            //map((hops(i).IP(0),hops(i-2).IP(0))) = counter
            counter+=1
            name + map((hops(i-1).IP,""))
          }
        }
      }
    }
    //println(map.mkString(" | "))
    (counter, map)
  }

  def filterAll(in: String, dns: String): mutable.HashSet[CaidaTraceroute] = {

    var set: mutable.HashSet[CaidaTraceroute] = Inference.dnsFilter(in, dns)
    println("In: "+set.size)
    //aliasResolution(set)
    set = Inference.ingressFilter(set, dns)
    println("Ingress: "+set.size)
    set = Inference.egressFilter(set, dns)
    println("Egress: "+set.size)
    set
  }

  def dnsFilter(in: String, dns: String) : mutable.HashSet[CaidaTraceroute] = {
    val fileIn = new File(in)
    val set = new mutable.HashSet[CaidaTraceroute]
    //var cnt = 0
    for(line <- Source.fromFile(fileIn).getLines() if line.contains(dns)){
      filterDNS(line, set, dns)
      //cnt += 1
      //if(cnt%1000 == 0) println("\r "+cnt)
    }
    set
  }
  def filterDNS(l: String, set: mutable.HashSet[CaidaTraceroute], dns: String): Unit = {
    if (!l.startsWith("#") && l != ""){
      val str = if(l.count(_ == "T ") > 1) {l.substring(0,l.indexOf("T ", 2))} else l

      var route = CaidaTraceroute.deserialize(str)
      set += route
    }
  }

  def ingressFilter(set: mutable.HashSet[CaidaTraceroute], dns: String) : mutable.HashSet[CaidaTraceroute] = {
    val storage = new mutable.HashSet[(String, String)]
    val newSet = new mutable.HashSet[CaidaTraceroute]
    for(traceroute <- set){
      val destination = traceroute.destination
      val ingress = traceroute.getIngress(dns)
      if(!storage.contains((destination, ingress))){
        storage += ((destination, ingress))
        newSet += traceroute
      }
    }
    newSet
  }

  def egressFilter(set: mutable.HashSet[CaidaTraceroute], dns: String) : mutable.HashSet[CaidaTraceroute] = {
    val storage = new mutable.HashSet[(String, String)]
    val newSet = new mutable.HashSet[CaidaTraceroute]
    for(traceroute <- set){
      val source = traceroute.source
      val egress = traceroute.getEgress(dns)
      if(!storage.contains((source, egress))){
        storage += ((source, egress))
        newSet += traceroute
      }
    }
    newSet
  }

}
