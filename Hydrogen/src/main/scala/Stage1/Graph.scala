package Stage1

import java.io.{BufferedWriter, File, FileWriter}

import org.graphstream.graph.implementations.{MultiGraph, MultiNode}
import org.graphstream.graph.{Edge, Node}

import scala.collection.mutable
import scala.io.Source

/**
  * Created by maximedemol on 16/06/16.
  */
object Graph {

  def buildGraphGUI(set: mutable.HashSet[CaidaTraceroute], dns: String, key: String): MultiGraph = {
    var last: CaidaHop = new CaidaHop(true)
    val graph = new MultiGraph("Topo")
    for(trace <- set){
      last = new CaidaHop(true)
      for(hop <- trace.hops if !hop.qVal){
        if(hop.DNS.contains(dns) && hop.DNS.contains(key)){
          val node = hop.DNS.substring(hop.DNS.indexOf(key))
          val name = if (node.contains("ANON")) hop.IP else node
          val lastnode = if(!last.qVal) last.DNS.substring(last.DNS.indexOf(key)) else ""
          val lastname: String = if (!last.qVal && lastnode.contains("ANON")) last.IP else lastnode
          val edge = lastname+"~>"+name
          if(graph.getNode(name)==null){
            val n: MultiNode = graph.addNode(name)
            n.addAttribute("ui.label", name)

          }
          if(!last.qVal && last.DNS.contains(dns) && last.DNS.contains(key) && graph.getEdge(edge)==null){
            graph.addEdge(edge, lastname, name, true)
          }
          last = hop
        }
      }
    }
    /*
      var nodes = new Array[Node](0)
      nodes = graph.getNodeSet().toArray(nodes)
      for(n: Node <- nodes){
        val deg = n.getDegree()
        if(deg < 2) graph.removeNode(n)
        else if (deg ==2){
          if (n.getLeavingEdgeSet.size == 1 &&  n.getEnteringEdgeSet.size == 1){
            val e1: Edge = n.getLeavingEdgeIterator.next()
            val e2: Edge = n.getEnteringEdgeIterator.next()
            if(e1.getTargetNode == e2.getSourceNode) graph.removeNode(n)
          }
        }
      }
      */

    var nodes = new Array[Node](0)
    nodes = graph.getNodeSet.toArray(nodes)
    for(n: Node <- nodes){
      val deg = n.getDegree
      if(deg == 0) graph.removeNode(n)
    }
    graph.addAttribute("ui.quality")
    graph.addAttribute("ui.antialias")
    graph
  }

  def printGraph(graph: MultiGraph, out: String): Unit = {
    val bw = new BufferedWriter(new FileWriter(new File(out)))
    bw.write(graph.getNodeCount+"\n")
    bw.write(graph.getEdgeCount+"\n")

    var nodes = new Array[Node](0)
    nodes = graph.getNodeSet.toArray(nodes)
    var edges = new Array[Edge](0)
    edges = graph.getEdgeSet.toArray(edges)

    for(n: Node <- nodes){
      bw.write(n.toString+" 0 0\n")
    }
    for(e: Edge <- edges){
      bw.write(e.toString.split("\\[")(0)+"\n")
    }
    bw.close()
  }

  def printTraces(graph: MultiGraph, in: String, out: String, key: String): Unit = {
    val fileIn = new File(in)
    var newline = false
    var inside = false
    val bw = new BufferedWriter(new FileWriter(new File(out)))
    var nodes = new Array[Node](0)
    nodes = graph.getNodeSet.toArray(nodes)
    val set = new mutable.HashSet[String]()
    nodes.foreach(n => set += n.toString)
    for(line <- Source.fromFile(fileIn).getLines()){
      if (!line.startsWith("#") && line != ""){
        val str = if(line.count(_ == "T ") > 1) {line.substring(0,line.indexOf("T ", 2))} else line
        val route = CaidaTraceroute.deserialize(str)
        for(hop <- route.hops if !hop.qVal) {
          val name = if (hop.DNS.contains(key)) hop.DNS.substring(hop.DNS.indexOf(key)) else hop.DNS
          if (set.contains(name)) {
            bw.write(name + " ")
            //println(name)
            newline = true
            inside = true
          } else if (set.contains(hop.IP)){
            bw.write(hop.IP+" ")
            //println(name)
            newline = true
            inside = true
          } else {
            if (inside){
              inside=false
              newline = false
              bw.write("\n")
            }
          }
        }
      }
      if(newline) {inside = false; newline = false; bw.write("\n")}
    }
    bw.close()
  }

}
