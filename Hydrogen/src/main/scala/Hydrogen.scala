import java.io.{File, FileWriter}

import Stage1.{CaidaTraceroute, DNS, Graph, Inference}
import Stage2.graphLib.topology.DGraph
import Stage2.solver.{InverseShortestPaths, InverseShortestPaths_control}
import Stage2.tests.New_Tests
import Stage2.utils.{DataPreProcessors, DataUtils, GraphAnalyser, Parser}

import scala.collection.mutable.ArrayBuffer

/**
  * Created by maximedemol on 16/06/16.
  */
object Hydrogen {

  def main(args: Array[String]): Unit = {
    println("Hello, world!")

    // Stage 1

    val warts = "warts/"
    val tracefile = "traces/Internet2.txt"
    val prefixes = Array("2001:468:")
    val dnsName = "internet2"
    val anon = "rtr.ANON.net.internet2.edu"
    val keyword = "rtr"
    val graphfile = "graph.txt"
    val graphtrace = "traces.txt"

    val dns = DNS.dnsNames("dns/")
    CaidaTraceroute.dumpFilter(warts, tracefile, prefixes, dns, dnsName, anon)
    val set = Inference.filterAll(tracefile,dnsName)
    val g = Graph.buildGraphGUI(set, dnsName ,keyword)

    Graph.printGraph(g,graphfile)
    Graph.printTraces(g,tracefile,graphtrace,keyword)
    g.display()

  }

  /*
		This method runs the first solver.

		INPUTS : source_dir: the source directory containing the traces (from which both the graph and the traceroutes can be taken)
				 output_dir: where to save the files
	*/

  def runSolver(source_dir:String, output_dir:String):Unit={
    val input_traces = s"$source_dir/traces.txt"
    val preprocessed_traces = s"$source_dir/preprocessed_traces.txt"

    //Pre process the traces using both hypotheses.
    DataPreProcessors.preProcess(true, true)(input_traces,preprocessed_traces)

    //Load the graph directly from the traces
    val graph = Parser.firstStageParsing(preprocessed_traces)

    //Load the traces routes into a path matrix
    val (traces,number) = DataUtils.loadPathMatrix(preprocessed_traces, graph)

    //Compute the weights using the firstt solver
    val (weights, errors) = InverseShortestPaths.optimize(traces,graph,number)

    //Write the computed weights
    val weights_writer = new FileWriter(new File(s"$output_dir/weights"))
    for(e <- graph.edges)
      weights_writer.write(e+" : "+weights(e.id)+"\n")
    weights_writer.close()

    //Write the error associated to each traceroute given in input
    val errors_writer = new FileWriter(new File(s"$output_dir/errors"))

    for(l <- traces; c <- l; p <- c){
      val tmp = ArrayBuffer[String]()
      var id = graph.edges(p.edges(0)).src.id
      tmp.append(graph.nodes(id).label)
      for(e <- p.edges){
        id = graph.edges(e).dest.id
        tmp.append(graph.nodes(id).label)
      }
      errors_writer.write(tmp.mkString(" ")+" : "+errors(p.id)+"\n")
    }
    errors_writer.close()
  }
  /*
  This function exactly reproduces the graph for which we are not able to compute
  the number of shortest paths added in addition to the ones observed.

  See report for more information.
  */
  def crash_test():Unit={
    val testDir = "crash_test"
    val graphFile = "crash_topo"

    //Parse the graph
    val graph = Parser.classicGraphParsing(s"$testDir/$graphFile")

    //Collect and write every possible shortest paths
    val (real,realNum)  = GraphAnalyser.shortestPaths(graph, e => e.weight)
    DataUtils.writePaths(real, graph, s"$testDir/real_traces")

    //Load path matrix from perfect information
    val (traces,number) = DataUtils.loadPathMatrix(s"$testDir/real_traces", graph)

    //Compute the weights using the second solver
    val (weights, error) = InverseShortestPaths_control.optimize(traces,graph,number)

    //Replace graph's weights by the one obtained
    for(edge <- graph.edges){
      edge.weight = weights(edge.id)
      println(edge + " : "+weights(edge.id))
    }

    //Collect and write every possible paths using the computed weights
    val (pred,predNum) = GraphAnalyser.shortestPaths(graph, e => e.weight)
    DataUtils.writePaths(pred, graph, s"$testDir/pred_traces")

  }

  //Method to run all the test presented in the paper
  def perform_tests():Unit={
    val resDirectory = "Results"
    checkDirecotyIfNotCreate(resDirectory)

    var lastTest = -1
    var lastGraph = -1
    val graphsDir = "topologies"
    val graphs = Array[(String,String => DGraph)](
      ("rf1221",Parser.classicGraphParsing),
      ("synth50", Parser.classicGraphParsing),
      ("synth100", Parser.classicGraphParsing),
      ("rf1755",Parser.classicGraphParsing),
      ("rf3967",Parser.classicGraphParsing)
    )

    val correct = Array[Double](0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9)

    for((graph,parser) <- graphs){
      New_Tests.in_out_test(s"$resDirectory/test-1-no-hyp", 5, graph, parser(graphsDir+"/"+graph),
        correct, correct, false, false, 1)
    }
    for((graph,parser) <- graphs){
      New_Tests.in_out_test(s"$resDirectory/test-1-sub", 5, graph, parser(graphsDir+"/"+graph),
        correct, correct, true, false, 1)
    }
    for((graph,parser) <- graphs){
      New_Tests.in_out_test(s"$resDirectory/test-1-reverse", 5, graph, parser(graphsDir+"/"+graph),
        correct, correct, false, true, 1)
    }
    for((graph,parser) <- graphs){
      New_Tests.in_out_test(s"$resDirectory/test-1-sub-reverse", 5, graph, parser(graphsDir+"/"+graph),
        correct, correct, true, true, 1)
    }
  }

  def checkDirecotyIfNotCreate(dir:String):Unit={
    val directory = new File(dir)
    if(!directory.exists())
      directory.mkdir()
  }

}
