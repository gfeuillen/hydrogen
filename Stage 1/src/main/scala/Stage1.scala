/**
  * Created by maximedemol on 16/06/16.
  */
object Stage1 {

  def main(args: Array[String]): Unit = {
    println("Hello, world!")

    val warts = "warts/"
    val tracefile = "traces/Internet2.txt"
    val prefixes = Array("2001:468:")
    val dnsName = "internet2"
    val anon = "rtr.ANON.net.internet2.edu"
    val keyword = "rtr"
    val graphfile = "graph.txt"
    val graphtrace = "traces.txt"

    val dns = DNS.dnsNames("dns/")
    CaidaTraceroute.dumpFilter(warts, tracefile, prefixes, dns, dnsName, anon)
    val set = Inference.filterAll(tracefile,dnsName)
    val g = Graph.buildGraphGUI(set, dnsName ,keyword)

    Graph.printGraph(g,graphfile)
    Graph.printTraces(g,tracefile,graphtrace,keyword)
    g.display()

  }

}
