import java.io.{BufferedWriter, File, FileWriter}

import scala.collection.mutable
import scala.io.Source

import sys.process._

import BetterString._

/**
  * Created by maximedemol on 16/06/16.
  */
class CaidaTraceroute(mKey: String, mSource: String, mDestination: String, mListID: Int, mCycleID: Long,
                      mTimestamp: Long, mDstReplied: String, mDstRTT: Double, mRequestTTL: Int, mReplyTTL: Int,
                      mHaltReason: String, mHaltReasonData: String, mPathComplete: String, mHops: Array[CaidaHop]) {

  var key: String = mKey
  var source: String = mSource
  var destination: String = mDestination
  var listID: Int = mListID
  var cycleID: Long = mCycleID
  var timestamp: Long = mTimestamp
  var dstReplied: String = mDstReplied
  var dstRTT: Double = mDstRTT
  var requestTTL: Int = mRequestTTL
  var replyTTL: Int = mReplyTTL
  var haltReason: String = mHaltReason
  var haltReasonData: String = mHaltReasonData
  var pathComplete: String = mPathComplete
  var hops: Array[CaidaHop] = mHops

  override def toString: String = {
    var ret = key + " " + source + " " + destination + " " + listID + " " + cycleID + " " + timestamp + " " +
      dstReplied + " " + dstRTT + " " + requestTTL + " " + replyTTL + " " + haltReason + " " + haltReasonData + " " +
      pathComplete + " "
    for (hop <- hops) ret = ret + hop + " "
    ret
  }

  def getIngress(dns: String) : String = {
    var ip = ""
    for(hop <- hops if ip == "" && !hop.qVal) {
      if (hop.DNS.contains(dns)){
        ip = hop.IP
      }
    }
    ip
  }

  def getEgress(dns: String) : String = {
    var ip = ""
    val rHops = hops.reverse
    for(hop <- rHops if ip == "" && !hop.qVal) {
      if (hop.DNS.contains(dns)){
        ip = hop.IP
      }
    }
    ip
  }

  def contains(dns: String): Boolean = {
    for(hop <- this.hops if !hop.qVal){
      if (hop.DNS.contains(dns)) return true
    }
    false
  }

}

class CaidaHop(mIP: String, mASN: Long, mDNS: String, mRTT: Double, mNTries: Int){

  var qVal = false
  var IP: String = mIP
  var ASN: Long = mASN
  var DNS: String = mDNS
  var RTT: Double = mRTT
  var nTries: Int = mNTries

  def this(q: Boolean) {
    this("", 0, "", 0.0, 0)
    qVal = true
  }

  override def toString: String = {
    if (qVal) return "q"
    IP + "," + ASN + "," +DNS+"," + RTT + "," + nTries + ";"
  }

}

object CaidaTraceroute {

  def deserialize(in: String): CaidaTraceroute = {

    val s = in.split("\\s+")
    val key = s(0)
    var source = ""
    var dest = ""
    try {
      source = s(1)
      dest = s(2)
    } catch {
      case e: ArrayIndexOutOfBoundsException => println(e); println(in)
    }
    var listId: Int = 0
    var cycleId: Long = 0
    var timestamp: Long = 0
    var destRep = ""
    var destRTT: Double = 0.0
    var reqTTL: Int = 0
    var repTTL: Int = 0
    var haltReason = ""
    var haltData = ""
    var complete = ""
    try {
      listId = s(3).toInt
      cycleId = s(4).toLong
      timestamp = s(5).toLong
      destRep = s(6)
      destRTT = s(7).toDouble
      reqTTL = s(8).toInt
      repTTL = s(9).toInt
      haltReason = s(10)
      haltData = s(11)
      complete = s(12)
    } catch {
      case e: NumberFormatException => println(e); println(in)
      case ea: ArrayIndexOutOfBoundsException => println(in)
    }

    val ips = s.slice(13, s.length)

    var hops: Array[CaidaHop] = Array[CaidaHop]()
    if(ips.length>0) {
      hops = CaidaHop.deserialize(ips)
    }


    new CaidaTraceroute(key, source, dest, listId, cycleId, timestamp, destRep, destRTT, reqTTL, repTTL, haltReason,
      haltData, complete, hops)

  }

  def deserialize(in: String, dns: mutable.HashMap[String, String], prefix: Array[String]): CaidaTraceroute = {

    val s = in.split("\\s+")
    val key = s(0)
    var source = ""
    var dest = ""
    try {
      source = s(1)
      dest = s(2)
    } catch {
      case e: ArrayIndexOutOfBoundsException =>  println(e); println(in)
    }
    var listId: Int = 0
    var cycleId: Long = 0
    var timestamp: Long = 0
    var destRep = ""
    var destRTT: Double = 0.0
    var reqTTL: Int = 0
    var repTTL: Int = 0
    var haltReason = ""
    var haltData = ""
    var complete = ""
    try {
      listId = s(3).toInt
      cycleId = s(4).toLong
      timestamp = s(5).toLong
      destRep = s(6)
      destRTT = s(7).toDouble
      reqTTL = s(8).toInt
      repTTL = s(9).toInt
      haltReason = s(10)
      haltData = s(11)
      complete = s(12)
    } catch {
      case e: NumberFormatException =>  println(e); println(in)
      case ea: ArrayIndexOutOfBoundsException => println(in)
    }

    val ips = s.slice(13, s.length)

    val hops = CaidaHop.deserialize(ips, dns, prefix)


    new CaidaTraceroute(key, source, dest, listId, cycleId, timestamp, destRep, destRTT, reqTTL, repTTL, haltReason,
      haltData, complete, hops)

  }

  def dumpFilter(in: String, out: String, prefix: Array[String], dns: mutable.HashMap[String, String], dnsName: String, name: String): Unit = {
    val file = new File(in)
    var cntF = 0
    var cntS = 0
    var cntT = 0
    val counter = 0
    //var ok: Long = 0
    //var nok: Long = 0
    val map = new mutable.HashMap[(String, String), Int]()
    var prev = (counter, map)
    val output = new File(out)
    //output.mkdir()
    output.createNewFile()
    val bw = new BufferedWriter(new FileWriter(output))
    val fad = new File("addresses.txt")
    fad.createNewFile()
    val bw2 = new BufferedWriter(new FileWriter(fad))
    val adset = new mutable.HashSet[String]()
    val fs = file.listFiles().length
    var set: mutable.HashSet[CaidaTraceroute] = new mutable.HashSet[CaidaTraceroute]
    for(w <- file.listFiles() if !w.getName.startsWith(".")) {
      val tmp = new File("tmp")
      tmp.createNewFile()
      ("sc_analysis_dump "+w) #> tmp!;
      Source.fromFile(tmp).getLines().toVector.foreach(l => {
        cntT += 1
        if (l.containsL(prefix)){
          val tmp = CaidaTraceroute.deserialize(l, dns, prefix)
          prev = Inference.completeRouters(tmp,prefix,dnsName, name, prev)
          /*
          for(hop <- tmp.hops if !hop.qVal){
            if(hop.DNS(0).contains("FAIL") || hop.DNS(0).contains("ANON")) nok += 1
            else ok += 1
          }
          */
          set += tmp
          cntS += 1
        }
      })
      tmp.delete()
      cntF += 1
      if(cntF%10==0){
        set.foreach(t => bw.write(t.toString()+"\n"))
        set = new mutable.HashSet[CaidaTraceroute]
      }
      println("Filtered file "+cntF+" of "+fs)
      //println(w.getName)
      //print(ok+", ")
      //println(nok+", ")
    }
    set.foreach(t => bw.write(t.toString()+"\n"))
    bw.close()
    println("Selected "+cntS+" traces out of "+cntT)
    for(ad <- map.keySet){
      adset += ad._1
    }
    for(ad <- adset){
      bw2.write(ad+"\n")
    }
    bw2.close()
  }

}

object CaidaHop {

  def deserialize(in: Array[String]): Array[CaidaHop] = {
    val hops = new Array[CaidaHop](in.length)
    for(i <- in.indices){
      val s = in(i).split(";")(0).split(",")
      val ip = s(0)
      try {
        if (ip != "q") {
          if (s.length == 3) hops(i) = new CaidaHop(s(0), 0, "", s(1).toDouble, s(2).toInt)
          else if (s.length == 4) hops(i) = new CaidaHop(s(0), s(1).toLong, "", s(2).toDouble, s(3).toInt)
          else if (s.length == 5) hops(i) = new CaidaHop(s(0), s(1).toLong, s(2), s(3).toDouble, s(4).toInt)
        } else {
          hops(i) = new CaidaHop(true)
        }
      } catch {
        case e : ArrayIndexOutOfBoundsException => println("error in deserializeBulk "+in(i))
      }
    }
    hops
  }

  def deserialize(in: Array[String], dns: mutable.HashMap[String, String], prefix: Array[String]): Array[CaidaHop] = {
    val hops = new Array[CaidaHop](in.length)
    for (i <- in.indices) {
      //val s = in(i).split(";")(0).split(",")
      var s: Array[String] = Array[String]()
      if (in(i).containsL(prefix)) {
        for (sp <- in(i).split(";")) {
          if (sp.containsL(prefix)) s = sp.split(",")
        }
      } else {
        s = in(i).split(";")(0).split(",")
      }
      val ip = s(0)
      try {
        if (ip != "q") {
          if (s.length == 3) {
            if (dns.contains(s(0))) hops(i) = new CaidaHop(s(0), 0, dns(s(0)), s(1).toDouble, s(2).toInt)
            else hops(i) = new CaidaHop(s(0), 0, "FAIL.NOT_FOUND", s(1).toDouble, s(2).toInt)
          }
          else if (s.length == 4) {
            if (dns.contains(s(0))) hops(i) = new CaidaHop(s(0), s(1).toLong, dns(s(0)), s(2).toDouble, s(3).toInt)
            else hops(i) = new CaidaHop(s(0), s(1).toLong, "FAIL.NOT_FOUND", s(2).toDouble, s(3).toInt)
          }
          else if (s.length == 5) hops(i) = new CaidaHop(s(0), s(1).toLong, s(2), s(3).toDouble, s(4).toInt)
        } else {
          hops(i) = new CaidaHop(true)
        }
      } catch {
        case e: ArrayIndexOutOfBoundsException => println("error in deserializeBulk " + in(i))
      }
    }
    hops
  }
}

class Counter(){
  var cnt: Double = 0

  def up(): Unit = cnt += 1
  def get(): Double = cnt

}