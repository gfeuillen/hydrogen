import java.io._

import scala.sys.process._

/**
  * Created by maximedemol on 01/05/16.
  */
object Warts {

  def wartsCat(in: String) : Unit = {
    val file = new File(in)
    val out = "warts/out.warts"
    var files = " "
    for(w <- file.listFiles() if !w.getName.startsWith(".")){
      println(w.getName)
      files += "warts/"+w.getName+" "
    }
    println(files)
    "sc_wartscat -o "+out + files !
  }

  def wartsTraces(in: String, out: String): Unit = {
    val file = new File(out)
    file.createNewFile()
    ("sc_analysis_dump "+in) #> file!
  }

  def deleteOld(in: String, limit: Int): Unit = {
    val file = new File(in)
    for (w <- file.listFiles() if !w.getName.startsWith(".")){
      val str = w.getName
      val date = str.split("\\.")(2).toInt
      if(date < limit){
        w.delete()
      }
    }
  }

}
