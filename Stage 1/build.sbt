name := "hydrogen_light"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies += "org.graphstream" % "gs-core" % "1.3"
libraryDependencies += "dnsjava" % "dnsjava" % "2.1.7"
libraryDependencies += "com.googlecode.java-ipv6" % "java-ipv6" % "0.16"
    