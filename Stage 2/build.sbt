lazy val root = (project in file(".")).
  settings(
    name := "oscar-cp template",
    scalaVersion := "2.11.4",
    resolvers += "Oscar Snapshots" at "http://artifactory.info.ucl.ac.be/artifactory/libs-release/",
    libraryDependencies += "oscar" %% "oscar-cp" % "3.0.1" withSources(),
    libraryDependencies += "net.sf.jung" % "jung2" % "2.0.1",
    libraryDependencies += "net.sf.jung" % "jung-algorithms" % "2.0.1",
    libraryDependencies += "net.sf.jung" % "jung-graph-impl" % "2.0.1",
    libraryDependencies += "net.sf.jung" % "jung-visualization" % "2.0.1"
  )
