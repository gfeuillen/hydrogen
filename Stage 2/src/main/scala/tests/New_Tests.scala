package test

import oscar.cp.network.graphLib.topology.DGraph
import simulator._
import java.io._

object New_Tests{
	def in_out_test(storage_dir:String, test_number:Int, graph_name:String, graph:DGraph, 
		inPr:Seq[Double], outPr:Seq[Double], sub_optimal:Boolean, reverse_optimal:Boolean, solver_type:Int){
		checkDirecotyIfNotCreate(storage_dir)
		val graph_dir = storage_dir+"/"+graph_name
		checkDirecotyIfNotCreate(graph_dir)
		val real_traces = graph_dir+"/real_traces"
		for(i <- 1 to test_number){
			val test_num_dir = graph_dir+"/"+i
			checkDirecotyIfNotCreate(test_num_dir)
			for(inNodes <- inPr; outNodes <- outPr){ 
				val test_dir = test_num_dir+"/"+"in_"+inNodes+"_out_"+outNodes
				checkDirecotyIfNotCreate(test_dir)
				//COMPUTE RANDOMLY IN AND OUT NODES ACCORDING TO PERCENTAGE
				val inNumber = math.ceil(graph.nNodes*(inNodes)).toInt
				var in = (graph.nodes map(node => node.label)).toList
				in = scala.util.Random.shuffle(in)
				in = in.takeRight(inNumber)
				val outNumber = math.ceil(graph.nNodes*(outNodes)).toInt
				var out = (graph.nodes map(node => node.label)).toList
				out = scala.util.Random.shuffle(out)
				out = out.takeRight(outNumber)

				//Create two events, for in and out, duration 1 is ok since we don't make more than one sample
				val in_Event = new classicNodesInUnreachableEvent(1,2,in)
				val out_Event = new classicNodesOutUnreachableEvent(1,2,out)

				val test_name = s"Test on graph: $graph_name using a percentage of nodes in: $inNodes and out: $outNodes"
				val out_traces = s"$test_dir/out_traces"
				val input_traces = s"$test_dir/input_traces"
				val preprocessed_traces = s"$test_dir/preprocessed_traces"
				val weights_file = s"$test_dir/weights"
				val errors_file = s"$test_dir/errors"
				val log_file = s"$test_dir/log"

				val test = new New_Test(test_name,graph,real_traces,out_traces,input_traces,preprocessed_traces,sub_optimal,
					reverse_optimal,solver_type,weights_file,errors_file,log_file,1,Seq[Event](in_Event,out_Event))

				test.perform()
			}

		}
	}

	// def linkDownTest(storage_dir:String, test_number:Int, graph_name:String, graph:DGraph, sampling_size:Int,
	// 	link:Int, sub_optimal:Boolean, reverse_optimal:Boolean, solver_type:Int):Unit={

	// 	checkDirecotyIfNotCreate(storage_dir)
	// 	val graph_dir = storage_dir+"/"+graph_name
	// 	checkDirecotyIfNotCreate(graph_dir)
	// 	val real_traces = graph_dir+"/real_traces"
	// 	for(i <- 1 to test_number){
	// 		val test_num_dir = graph_dir+"/"+i
	// 		checkDirecotyIfNotCreate(test_num_dir)
	// 		for(inNodes <- inPr; outNodes <- outPr){ 
	// 			val test_dir = test_num_dir+"/"+"in_"+inNodes+"_out_"+outNodes
	// 			checkDirecotyIfNotCreate(test_dir)
	// 			//COMPUTE RANDOMLY IN AND OUT NODES ACCORDING TO PERCENTAGE
	// 			val inNumber = math.ceil(graph.nNodes*(inNodes)).toInt
	// 			var in = (graph.nodes map(node => node.label)).toList
	// 			in = scala.util.Random.shuffle(in)
	// 			in = in.takeRight(inNumber)
	// 			val outNumber = math.ceil(graph.nNodes*(outNodes)).toInt
	// 			var out = (graph.nodes map(node => node.label)).toList
	// 			out = scala.util.Random.shuffle(out)
	// 			out = out.takeRight(outNumber)

	// 			//Create two events, for in and out, duration 1 is ok since we don't make more than one sample
	// 			val in_Event = new classicNodesInUnreachableEvent(1,sampling_size+1,in)
	// 			val out_Event = new classicNodesOutUnreachableEvent(1,sampling_size+1,out)
	// 			//create random edge
	// 			val random_edge_id = scala.util.Random.nextInt(graph.nEdges)
	// 			val random_edge = graph.edge(random_edge_id)
	// 			val src = random_edge.src.label
	// 			val dest = random_edge.dest.label

	// 			//Create two events, for in and out, duration 1 is ok since we don't make more than one sample
	// 			val link_down_event = new classicLinkDownEvent(1,2,s"$src-$dest")

	// 			val test_name = s"Test on graph: $graph_name using one link down at first sampling"
	// 			val out_traces = s"$test_dir/out_traces"
	// 			val input_traces = s"$test_dir/input_traces"
	// 			val preprocessed_traces = s"$test_dir/preprocessed_traces"
	// 			val weights_file = s"$test_dir/weights"
	// 			val errors_file = s"$test_dir/errors"
	// 			val log_file = s"$test_dir/log"

	// 			val test = new New_Test(test_name,graph,real_traces,out_traces,input_traces,preprocessed_traces,sub_optimal,
	// 				reverse_optimal,solver_type,weights_file,errors_file,log_file,sampling_size,Seq[Event](link_down_event))

	// 			test.perform()
	// 		}

	// 	}

	// }
	def checkDirecotyIfNotCreate(dir:String):Unit={
		val directory = new File(dir)
		if(!directory.exists())
			directory.mkdir()
	}
}
