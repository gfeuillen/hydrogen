package simulator
import oscar.cp.network.graphLib.topology.DGraph
import misc.Path

trait PreEvent extends Event{
	val in: DGraph => Unit
	val out: DGraph => Unit
	def apply(graph : DGraph):Unit={
		in(graph)
	}
	def revert(graph : DGraph):Unit={
		out(graph)
	}
}

trait PostEvent extends Event{
	val postTreatment: Array[Array[Array[Path]]] => Array[Array[Array[Path]]]
	def apply(paths:Array[Array[Array[Path]]]):Array[Array[Array[Path]]]={
		postTreatment(paths)
	}
}