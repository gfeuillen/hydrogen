package simulator

import oscar.cp.network.graphLib.topology.DGraph
import utils.DataFilters

class probaLinkDownEvent(prob:Double, link:String, dur:Int=1) 
	extends PreEvent
	with ProbaEvent{

	val duration = dur
	val in = EventFunctions.linkDown(link)_
	val out = EventFunctions.linkUp(link)_
	val proba = prob

	stateString = "Probabilistic event link down : "+link
}

class probaNodeDownEvent(prob:Double, node:String, dur:Int=1)
	extends PreEvent
	with ProbaEvent{

	val duration = dur
	val in = EventFunctions.nodeDown(node)_
	val out = EventFunctions.nodeUp(node)_
	val proba = prob

	stateString = "Probabilistic event node down : "+node
}

class probaNodeUnreachableEvent(prob:Double, node:String, dur:Int=1)
	extends PostEvent
	with ProbaEvent{

	val duration = dur
	val proba = prob
	val postTreatment = DataFilters.inOutFilter(Array[String](node), Array[String](node),  Array[(String,String)](), dgraph)_

	stateString = "Probabilistic event node unreachable : "+node
}
class classicLinkDownEvent(s:Int, e:Int, link:String, dur:Int=1) 
	extends PreEvent
	with ClassicEvent{

	val duration = dur
	val in = EventFunctions.linkDown(link)_
	val out = EventFunctions.linkUp(link)_
	val start = s
	val end = e

	stateString = "Classic event link down : "+link
}

class classicNodeDownEvent(s:Int, e:Int, node:String, dur:Int=1)
	extends PreEvent
	with ClassicEvent{

	val duration = dur
	val in = EventFunctions.nodeDown(node)_
	val out = EventFunctions.nodeUp(node)_
	val start = s
	val end = e

	stateString = "Classic event node down : "+node
}

class classicNodeUnreachableEvent(s:Int, e:Int, node:String, dur:Int=1)
	extends PostEvent
	with ClassicEvent{

	val duration = dur
	val start = s
	val end = e
	val postTreatment = DataFilters.inOutFilter(Array[String](node), Array[String](node),  Array[(String,String)](), dgraph)_

	stateString = "Classic event node unreachable : "+node
}

class classicNodesInUnreachableEvent(s:Int, e:Int, nodes:Seq[String], dur:Int=1)
	extends PostEvent
	with ClassicEvent{

	val duration = dur
	val start = s
	val end = e
	val postTreatment = DataFilters.inOutFilter(nodes.toArray, Array[String](),  Array[(String,String)](), dgraph)_

	stateString = "Classic event nodes in unreachable : "+nodes.mkString(" ")
}

class classicNodesOutUnreachableEvent(s:Int, e:Int, nodes:Seq[String], dur:Int=1)
	extends PostEvent
	with ClassicEvent{

	val duration = dur
	val start = s
	val end = e
	val postTreatment = DataFilters.inOutFilter(Array[String](), nodes.toArray,  Array[(String,String)](), dgraph)_

	stateString = "Classic event nodes out unreachable : "+nodes.mkString(" ")
}