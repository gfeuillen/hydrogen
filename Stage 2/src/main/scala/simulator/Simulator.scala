package simulator

import scala.collection.mutable.ArrayBuffer
import java.io._
import oscar.cp.network.graphLib.topology.DGraph
import utils.GraphAnalyser

class Simulator(graph:DGraph, logfile:String, tracerouteFile:String, sampleSize : Int = 100){

	//Store events
	val preEvents = ArrayBuffer[PreEvent]()
	//Toggle : true if event has been activated, false if not
	val preToggles = ArrayBuffer[Boolean]()

	//Store events
	val postEvents = ArrayBuffer[PostEvent]()

	val logFile = new File(logfile)
	val logWriter = new PrintWriter(logFile)

	val traceFile = new File(tracerouteFile)
	val traceWriter = new FileWriter(traceFile)

	def addPreEvent(e:PreEvent): Unit={
		//TODO check if event is in bound ? MIght be done using totalTime given ?

		//Init the event totalTime (in case of cyclic event with percentage time)
		e.setTotalTime(sampleSize)
		//Add the event to the list
		preEvents += e
		preToggles += false
	}

	def addPostEvent(e:PostEvent): Unit={
		//TODO check if event is in bound ? MIght be done using totalTime given ?

		//Init the event totalTime (in case of cyclic event with percentage time)
		e.setTotalTime(sampleSize)
		e.setGraph(graph)
		//Add the event to the list
		postEvents += e
	}

	def addEvent(e:Event):Unit = {
		e match {
			case event: PreEvent => addPreEvent(event)
  			case event: PostEvent => addPostEvent(event)
  			case _ => println("Error could not add event, please use Pre/Post event as trait !")
  		}
	}

	def simulate():Unit={
		for (time <- 1 to sampleSize) {
			logWriter.write("-----------------------------"+"\n")
			logWriter.write("TIME : "+time+"\n")
			logWriter.write("-----------------------------"+"\n")
			//Apply or revert the events
			for (i <- 0 until preEvents.length) {
				val e = preEvents(i)
				val toggle = preToggles(i)
				val on = e.isOn(time)
				// println("EVENT : "+e.toString())
				//If it should be on, but not yet on
				if(on && !toggle){
					//Apply the function on the graph
					e.apply(graph)
					//Switch the toggle
					preToggles(i) = !toggle
				}
				//If it should be off, but not yet off
				else if (!on && toggle){
					//Revert back to previous graph
					e.revert(graph)
					//Switch the toggle
					preToggles(i) = !toggle
				}
				logWriter.write(e.toString()+" : "+on+"\n")
			}
			sample(time)
		}
		logWriter.close()
		traceWriter.close()
		//Empty the downedges
		graph.downEdges = ArrayBuffer[Int]()
	}
	def sample(time:Int):Unit={
		//Get the traces
		val (paths, tot) = GraphAnalyser.shortestPaths(graph, e => e.weight)
		var shortestPaths = paths

		//Apply post events

		for (i <- 0 until postEvents.length) {
			val e = postEvents(i)
			val on = e.isOn(time)
			//If it should be on, but not yet on
			if(on){
				//Apply the function on the graph
				shortestPaths = e.apply(shortestPaths)
			}
			logWriter.write(e.toString()+" : "+on+"\n")
		}

		//Write them
		for(l <- shortestPaths; c <- l; p <- c){
			// println(p.edges.mkString(" "))
			val tmp = ArrayBuffer[String]()
			var id = graph.allEdges(p.edges(0)).src.id
			tmp.append(graph.nodes(id).label)
			for(e <- p.edges){
				id = graph.allEdges(e).dest.id
				tmp.append(graph.nodes(id).label)
			}
			//tmp.append(p.dest)
			traceWriter.write(tmp.mkString(" ")+"\n")
		}
	}
}