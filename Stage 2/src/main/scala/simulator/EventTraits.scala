package simulator

import oscar.cp.network.graphLib.topology.DGraph
trait ProbaEvent{ 
	val proba:Double

	def startCondition(time:Int):Boolean={
		val r = scala.util.Random
		val rand = r.nextInt(100)
		// println("RANDOM : "+rand)
		if(rand <= proba*100)
			true
		else
			false
	}
}
trait ClassicEvent{
	val start:Int
	val end:Int

	def startCondition(time:Int):Boolean={
		if(time >= start && time < end)
			true
		else
			false
	}
}

// 	//Constructor if given percentage of the total duration
// 	//def this(start:Double, end:Double, period:Double, in:DGraph => Unit, out:DGraph => Unit, duration: Int = 1) 
// 	//	= this(math.ceil(start*this.totalTime).toInt, math.ceil(end*totalTime).toInt, math.ceil(period*totalTime).toInt, in,out,duration)

// 	def startCondition(time:Int):Boolean={
// 		if((time-start)%period == 0)
// 			true
// 		else
// 			false
// 	}
// }

