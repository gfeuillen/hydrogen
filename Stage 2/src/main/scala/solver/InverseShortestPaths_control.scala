
package solver

import gurobi.GRBEnv
import gurobi.GRBModel
import gurobi.GRB
import gurobi.GRBLinExpr
import gurobi.GRBVar
import gurobi.GRBQuadExpr
import oscar.cp.network.graphLib.topology.DGraph
import scala.collection.mutable.ArrayBuffer
import misc.Path
import collection.mutable.HashMap

import oscar.cp.network.gurobiLib._

object InverseShortestPaths_control {



    def optimize(pathsMatrix: Array[Array[Array[Path]]], topology: DGraph, pathNumber:Int): (Array[Double],Array[Double]) = {
        optimize(pathsMatrix, topology.nNodes, topology.nEdges, pathNumber)
    }

    def optimize(pathsMatrix: Array[Array[Array[Path]]], nNodes: Int, nEdges: Int, pathNumber:Int): (Array[Double], Array[Double]) = {

        

        println("BEGINNING TO OPTIMIZE")
        val Nodes = 0 until nNodes
        val Edges = 0 until nEdges

        val minWeight = 1
        val maxWeight = 1 // 2^8

        // Gurobi model
        val env: GRBEnv = new GRBEnv()
        implicit val model: GRBModel = new GRBModel(env)

        // Edge weight variables
        val weights = Array.tabulate(nEdges)(e => integerVar(minWeight, maxWeight, "weight(" + e + ")"))
    
        // Error in path length
        val pathError = Array.tabulate(pathNumber)(i => integerVar(0, M, "error id : "+i))

        // Path length variables
        val pathLengths = Array.tabulate(pathNumber)(i => integerVar(minWeight, maxWeight * nEdges, "length id :"+i))

        //Path length with errors, easy to precompute
        val pathLengthsWithErrors = Array.tabulate(pathNumber)(i => integerVar(minWeight, maxWeight * nEdges, "length id :"+i))

        //Store R1s (P SiD, P Si'I, P Si''D)
        val r1s = HashMap[(String,String,String), GRBVar]()
        //Store R2s (P SiD, P Si'I, P Si''D)
        val r2s = HashMap[(Int,Int), GRBVar]()
        //Store R1*R2 to keep it linear (P SiD, P Si'I, P Si''D)
        val rs = HashMap[(String,String,String), GRBVar]()

        val qRels = HashMap[(String,String,String), GRBVar]()

        val qSDsLin = HashMap[(Int,Int), GRBLinExpr]()

        val qSDs = HashMap[(Int,Int), GRBVar]()

        val pathToSID = HashMap[(String,String), (Int,Int,Int)]()

        val oneObsIsShortestLin = HashMap[(Int,Int), GRBQuadExpr]()
        val oneObsIsShortest = HashMap[(Int,Int), GRBVar]()

        val tricks = ArrayBuffer[(GRBVar, GRBQuadExpr)]()

        val opposeds = ArrayBuffer[(GRBVar,GRBLinExpr)]()

        var equalityRS = ArrayBuffer[GRBVar]()

        def multiplyBoolean(variables:Array[GRBVar], name:String):GRBVar={
            if(variables.size > 1){
                var lastVar = variables(0)
                for(i <- 1 until variables.size){
                    val trickVar = booleanVar(name)
                    tricks += ((trickVar, lastVar*variables(i)))
                    lastVar = trickVar
                }
                return lastVar
            }
            else{
                return variables.head
            }
        }

        def reversedBoolean(variable:GRBVar):GRBVar={
            val reversedVar = booleanVar("Reversed var")
            val tmp = variable - 1
            val reversedExpr = tmp.opposed
            opposeds += ((reversedVar, reversedExpr))
            reversedVar
        }

        val equalities = ArrayBuffer[GRBQuadExpr]()
        val inequalities = ArrayBuffer[GRBLinExpr]()


        val totError = integerVar(0, M, "totError")

        model.update() // close the set of variables
    
        // Total error
        val errorSum = new GRBLinExpr()
        for(i <- Nodes; j <- Nodes){
            for(p <- pathsMatrix(i)(j)){
                errorSum += pathError(p.id)*p.count
            }
        }
        totError === errorSum

        // Path length constraints
        for (src <- Nodes; dest <- Nodes; if !pathsMatrix(src)(dest).isEmpty) {
            for(p <- pathsMatrix(src)(dest)){
                val length = sum(p.edges)(edge => weights(edge))
                length === pathLengths(p.id)
                val lengthWithError = length - pathError(p.id)
                lengthWithError === pathLengthsWithErrors(p.id)
            }
        }

        // Shortest path constraints
        for (src <- Nodes; dest <- Nodes; if !pathsMatrix(src)(dest).isEmpty) { 

            //ECPMS constraints
            for(i <- 0 until pathsMatrix(src)(dest).size; j <- i+1 until pathsMatrix(src)(dest).size){
                pathLengthsWithErrors(pathsMatrix(src)(dest)(i).id) === pathLengthsWithErrors(pathsMatrix(src)(dest)(j).id)
                // println("CONSTRAINT : "+pathsMatrix(src)(dest)(i).edges.mkString(" ") + "==" +pathsMatrix(src)(dest)(j).edges.mkString(" "))

            }

            //ECMPs variable indicator
            // oneObsIsShortestLin.put((src,dest), new GRBQuadExpr())
            // oneObsIsShortest.put((src,dest), booleanVar(s"A shortest path from $src to $dest"))


            val path_shortests = ArrayBuffer[GRBVar]()
            for(p <- pathsMatrix(src)(dest)){
                val error = pathError(p.id)
                val r = booleanVar(s"Shortest path $src -> $dest")

                //set up equality
                val equality = error*r
                equality -= error
                equalities += equality

                //set up inequality
                val inequality = error+1-r
                inequalities += inequality
                path_shortests += r

                equalityRS+= r
            }
            val tmpR2 = multiplyBoolean(path_shortests.toArray, s"Shortest path $src -> $dest")
            val r2 = reversedBoolean(tmpR2)
            r2s.put((src,dest), r2)
            
            //Check if a constraint has been set up for the source S and destination D
            var constraint_set = false

            //Here we take the first one only, thanks to ECMPS other constraints are also set up
            for (inter <- Nodes if inter != src && inter != dest && !pathsMatrix(src)(inter).isEmpty && !pathsMatrix(inter)(dest).isEmpty)  {
                for (j <- 0 until pathsMatrix(src)(inter).size ;k <- 0 until pathsMatrix(inter)(dest).size){
                    
                    val p2 = pathsMatrix(src)(inter)(j)
                    val p3 = pathsMatrix(inter)(dest)(k)

                    val tmpPath = new Path(src, dest, p2.edges ++ p3.edges, 1, 1)
                    //Check that this path is not already on the left side of the equation
                    if(! pathsMatrix(src)(dest).contains(tmpPath)){
                        //

                        //Set up the normal constraint using the error
                        val p1 = pathsMatrix(src)(dest)(0)

                        val pathLength = pathLengthsWithErrors(p2.id) + pathLengthsWithErrors(p3.id)
                        pathLength >= pathLengthsWithErrors(p1.id)


                        //In this case we add the constraint and need to set up R1 and R2 as well as creating R1*R2 and storing it
                        val p1Str = p1.edges.mkString(" ")
                        val p2Str = p2.edges.mkString(" ")
                        val p3Str = p3.edges.mkString(" ")

                        println(s"CONSTRAINT : $p1Str <= $p2Str + $p3Str")

                         // println(s"Setting up $p1Str <= $p2Str+$p3Str")

                        //SET R1
                        val length1 = pathLengths(p1.id)
                        val length2 = pathLengths(p2.id)
                        val length3 = pathLengths(p3.id)
                        
                        val r1 = booleanVar(s"r1 $p1Str <= $p2Str + $p3Str")
                        //save it
                        r1s.put((p1Str,p2Str,p3Str),r1)


                        val tmp1 = length1*r1
                        val tmp2 = length2*r1
                        val tmp3 = length3*r1

                        var equality1 = tmp2 + tmp3
                        equality1 = equality1 - tmp1

                        equalities += equality1

                        val inequality1 = length2+length3-length1+r1
                        inequalities += inequality1
                        //SET R2
                        val p1Error = pathError(p1.id)

                        // val r2 = booleanVar(s"r2 $p1Str <= $p2Str + $p3Str")
                        // //save it
                        // r2s.put((p1Str,p2Str,p3Str),r2)
                        // val equality2 = p1Error*r2
                        // equalities += equality2

                        // val inequality2 = p1Error+r2
                        // inequalities += inequality2

                        //Create and save the R1*R2 variable
                        val r1r2 = booleanVar(s"r $p1Str <= $p2Str + $p3Str")
                        rs.put((p1Str,p2Str,p3Str),r1r2)

                        val qRel = integerVar(0, pathNumber,s"q $p1Str <= $p2Str + $p3Str")
                        qRels.put((p1Str,p2Str,p3Str), qRel)

                        var qSD = qSDsLin getOrElse((src,dest), new GRBLinExpr())
                        qSD += qRel
                        qSDsLin.put((src,dest),qSD)
                        val qSD2 = integerVar(0, pathNumber * nEdges, "QSD")
                        qSDs.put((src,dest), qSD2)

                        pathToSID.put((p2Str,p3Str), (src,inter,dest))
                    }
                }
            }
            // if (constraint_set)
            //     qs.put((src,dest),integerVar(0, pathNumber,s"q $src $dest"))

        }

        var total = new GRBLinExpr()

        model.update()

        println("Been there")

        for(equality <- equalities)
            equality === 0
        for(inequality <- inequalities)
            inequality >= 1
        for((p1,p2,p3) <- rs.keys){
            val (s,_,d) = pathToSID((p2,p3))
            val tmp = r1s((p1,p2,p3))*r2s((s,d))
            tmp === rs((p1,p2,p3))
        }

        println("Done that")
        

        for(key <- qSDsLin.keys){
            // val qSD = integerVar(0, pathNumber * nEdges, "QSD")
            val qSD = qSDs(key)
            qSD === qSDsLin(key)
            // qSDs.put(key, qSD)
            total += qSD

            qSD <= 3
        }

        println("Done that too")
        for((p1,p2,p3) <- qRels.keys){
            // println("Begin")
            val rRel = rs((p1,p2,p3))
            val qRel = qRels((p1,p2,p3))

            val (s,i,d) = pathToSID((p2,p3))

            val tmp = new GRBQuadExpr()
            tmp += rRel
            // tmp -= 1

            // println("middle")

            if(qSDs.contains((s,i))){
                val qSI = qSDs((s,i))
                val tmp1 = rRel*qSI
                tmp += tmp1
            }
            if(qSDs.contains((i,d))){
                val qID = qSDs((i,d))
                val tmp2 = rRel*qID
                tmp += tmp2
            }
            // println("end")
            tmp === qRel
        } 

        for((var1, var2) <- tricks){
            var2 === var1
        }
        for((var1, expr1) <- opposeds){
            var1 === expr1
        }

        println("LET'S MINIMIZE")

        minimize(errorSum)

        val assignedWeights = Array.tabulate(nEdges)(edge => math.round(weights(edge).value).toDouble)
        val assignedErrors = Array.tabulate(pathNumber)(i => pathError(i).value)
        // for(key <- rs.keys){
        //     println(rs(key).name + " : "+rs(key).value)
        // }

        // for(k <- r2s.keys){
        //     val v = r2s(k)
        //     println(v.name + " : " +v.value)
        // }
        
        for(k <- qRels.keys){
            val v = qRels(k)
            println("Constraint : "+v.name + " : " +v.value)
        }
        for((s,d) <- qSDs.keys){
            println(s"More for $s -> $d" + " : "+qSDs((s,d)).value)
        }
        println("Total more are : "+total.getValue())


        model.dispose()
        env.dispose()

        (assignedWeights, assignedErrors)
    }
    def filterPaths(pathsMatrix: Array[Array[Array[Path]]], errors:Array[Double]) : Array[Array[Array[Path]]]={
        val n = pathsMatrix.size
        val filtered = Array.fill(n, n)(Array[Path]())
        for(src <- 0 until n ; dest <- 0 until n ; p <- pathsMatrix(src)(dest)){
            //TODO : CHANGE TO GET THE REAL VALUE
            if(errors(p.id)==0.0)
                filtered(src)(dest) = filtered(src)(dest) :+ p
        }
        filtered
    }
}