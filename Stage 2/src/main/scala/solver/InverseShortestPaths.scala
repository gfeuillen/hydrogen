package solver

import gurobi.GRBEnv
import gurobi.GRBModel
import gurobi.GRB
import gurobi.GRBLinExpr
import gurobi.GRBVar
import gurobi.GRBQuadExpr
import oscar.cp.network.graphLib.topology.DGraph
import misc.Path

import oscar.cp.network.gurobiLib._

object InverseShortestPaths {

	def optimize(pathsMatrix: Array[Array[Array[Path]]], topology: DGraph, pathNumber:Int): (Array[Double],Array[Double]) = {
		optimize(pathsMatrix, topology.nNodes, topology.nEdges, pathNumber)
	}

	def optimize(pathsMatrix: Array[Array[Array[Path]]], nNodes: Int, nEdges: Int, pathNumber:Int): (Array[Double], Array[Double]) = {

        // println("BEGINNING TO OPTIMIZE")
		val Nodes = 0 until nNodes
		val Edges = 0 until nEdges

		val minWeight = 1
		val maxWeight = 1 << 8 // 2^8

    	// Gurobi model
    	val env: GRBEnv = new GRBEnv("/home/gauthier/Thesis/CP/Gurobi/res/gurobi.log")
    	implicit val model: GRBModel = new GRBModel(env)

    	// Edge weight variables
    	val weights = Array.tabulate(nEdges)(e => integerVar(minWeight, maxWeight, "weight(" + e + ")"))
    
   		// Error in path length
    	val pathError = Array.tabulate(pathNumber)(i => integerVar(0, M, "error id : "+i))

    	// Path length variables
    	val pathLengths = Array.tabulate(pathNumber)(i => integerVar(minWeight, maxWeight * nEdges, "length id :"+i))
    
    	val totError = integerVar(0, M, "totError")

    	model.update() // close the set of variables
    
    	// Total error
    	val errorSum = new GRBLinExpr()
    	for(i <- Nodes; j <- Nodes){
    		for(p <- pathsMatrix(i)(j)){
    			errorSum += pathError(p.id)*p.count
    		}
    	}
    	totError === errorSum

    	// Path length constraints
    	for (src <- Nodes; dest <- Nodes; if !pathsMatrix(src)(dest).isEmpty) {
    		for(p <- pathsMatrix(src)(dest)){
    			val length = sum(p.edges)(edge => weights(edge))
    			length -= pathError(p.id)
    			length === pathLengths(p.id)
    		}
    	}

    	// Shortest path constraints
    	for (src <- Nodes; dest <- Nodes; if !pathsMatrix(src)(dest).isEmpty) {	
    		//ECPMS
    		for(i <- 0 until pathsMatrix(src)(dest).size; j <- i+1 until pathsMatrix(src)(dest).size){
                // println("ECMPS : "+src+" "+dest)
    			pathLengths(pathsMatrix(src)(dest)(i).id) === pathLengths(pathsMatrix(src)(dest)(j).id)
            }
    		
            //Here we take the first one only, thanks to ECMPS other constraints are also set up
            for (inter <- Nodes if inter != src && inter != dest && !pathsMatrix(src)(inter).isEmpty && !pathsMatrix(inter)(dest).isEmpty)  {
    	    	// println("CONSTRAINT : "+src+" "+dest+" "+inter)
                // Adds the path length constraint
    	    	val pathLength = pathLengths(pathsMatrix(src)(inter)(0).id) + pathLengths(pathsMatrix(inter)(dest)(0).id)
    	    	pathLength >= pathLengths(pathsMatrix(src)(dest)(0).id)
    		}
		}

        // println("LET'S MINIMIZE")

		minimize(errorSum)

		val assignedWeights = Array.tabulate(nEdges)(edge => weights(edge).value)
        val assignedErrors = Array.tabulate(pathNumber)(i => pathError(i).value)

		model.dispose()
		env.dispose()

		(assignedWeights, assignedErrors)
	}
	def filterPaths(pathsMatrix: Array[Array[Array[Path]]], errors:Array[Double]) : Array[Array[Array[Path]]]={
		val n = pathsMatrix.size
		val filtered = Array.fill(n, n)(Array[Path]())
		for(src <- 0 until n ; dest <- 0 until n ; p <- pathsMatrix(src)(dest)){
            //TODO : CHANGE TO GET THE REAL VALUE
			if(errors(p.id)==0.0)
				filtered(src)(dest) = filtered(src)(dest) :+ p
        }
        filtered
	}
}