package misc

import oscar.visual.VisualFrame
import simulator.Simulator
import simulator.probaLinkDownEvent
import simulator.probaNodeDownEvent
import simulator.probaNodeUnreachableEvent
import simulator.classicLinkDownEvent
import simulator.classicNodeDownEvent
import simulator.classicNodeUnreachableEvent
import scala.collection.mutable.ArrayBuffer
import java.io._
import oscar.cp.network.graphLib.topology.DGraph
import scala.io.Source
import utils.Parser
import utils.DataUtils
import utils.GraphAnalyser
import utils.DataPreProcessors
import solver._
import test.New_Tests
import performances.perf_writer

object Main extends App {
	main()

	def main():Unit={
		//Change what is here to run the desired method
	}

	/*
		This method runs the first solver.

		INPUTS : source_dir: the source directory containing the traces (from which both the graph and the traceroutes can be taken)
				 output_dir: where to save the files
	*/

	def runSolver(source_dir:String, output_dir:String):Unit={
		val input_traces = s"$source_dir/traces.txt"
		val preprocessed_traces = s"$source_dir/preprocessed_traces.txt"

		//Pre process the traces using both hypotheses.
		DataPreProcessors.preProcess(true, true)(input_traces,preprocessed_traces)

		//Load the graph directly from the traces
		val graph = Parser.firstStageParsing(preprocessed_traces)

		//Load the traces routes into a path matrix
		val (traces,number) = DataUtils.loadPathMatrix(preprocessed_traces, graph)
		
		//Compute the weights using the firstt solver
		val (weights, errors) = InverseShortestPaths.optimize(traces,graph,number)

		//Write the computed weights
		val weights_writer = new FileWriter(new File(s"$output_dir/weights"))
		for(e <- graph.edges)
			weights_writer.write(e+" : "+weights(e.id)+"\n")
		weights_writer.close()

		//Write the error associated to each traceroute given in input
		val errors_writer = new FileWriter(new File(s"$output_dir/errors"))

		for(l <- traces; c <- l; p <- c){
			val tmp = ArrayBuffer[String]()
			var id = graph.edges(p.edges(0)).src.id
			tmp.append(graph.nodes(id).label)
			for(e <- p.edges){
				id = graph.edges(e).dest.id
				tmp.append(graph.nodes(id).label)
			}
			errors_writer.write(tmp.mkString(" ")+" : "+errors(p.id)+"\n")
		}
		errors_writer.close()
	}
	/*
	This function exactly reproduces the graph for which we are not able to compute
	the number of shortest paths added in addition to the ones observed. 

	See report for more information.
	*/
	def crash_test():Unit={
		val testDir = "crash_test"
		val graphFile = "crash_topo"

		//Parse the graph
		val graph = Parser.classicGraphParsing(s"$testDir/$graphFile")

		//Collect and write every possible shortest paths
		val (real,realNum)  = GraphAnalyser.shortestPaths(graph, e => e.weight)
		DataUtils.writePaths(real, graph, s"$testDir/real_traces")

		//Load path matrix from perfect information
		val (traces,number) = DataUtils.loadPathMatrix(s"$testDir/real_traces", graph)

		//Compute the weights using the second solver
		val (weights, error) = InverseShortestPaths_control.optimize(traces,graph,number)

		//Replace graph's weights by the one obtained
		for(edge <- graph.edges){
			edge.weight = weights(edge.id)
			println(edge + " : "+weights(edge.id))
		}

		//Collect and write every possible paths using the computed weights
		val (pred,predNum) = GraphAnalyser.shortestPaths(graph, e => e.weight
		DataUtils.writePaths(pred, graph, s"$testDir/pred_traces")

	}

	//Method to run all the test presented in the paper
	def perform_tests():Unit={
		val resDirectory = "Results"
		checkDirecotyIfNotCreate(resDirectory)

		var lastTest = -1
		var lastGraph = -1
		val graphsDir = "topologies"
		val graphs = Array[(String,String => DGraph)](
			("rf1221",Parser.classicGraphParsing),
			("synth50", Parser.classicGraphParsing),
			("synth100", Parser.classicGraphParsing),
			("rf1755",Parser.classicGraphParsing),
			("rf3967",Parser.classicGraphParsing)
		)

		val correct = Array[Double](0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9)

		for((graph,parser) <- graphs){
			New_Tests.in_out_test(s"$resDirectory/test-1-no-hyp", 5, graph, parser(graphsDir+"/"+graph), 
		correct, correct, false, false, 1)
		}
		for((graph,parser) <- graphs){
			New_Tests.in_out_test(s"$resDirectory/test-1-sub", 5, graph, parser(graphsDir+"/"+graph), 
		correct, correct, true, false, 1)
		}
		for((graph,parser) <- graphs){
			New_Tests.in_out_test(s"$resDirectory/test-1-reverse", 5, graph, parser(graphsDir+"/"+graph), 
		correct, correct, false, true, 1)
		}
		for((graph,parser) <- graphs){
			New_Tests.in_out_test(s"$resDirectory/test-1-sub-reverse", 5, graph, parser(graphsDir+"/"+graph), 
		correct, correct, true, true, 1)
		}
	}

	def checkDirecotyIfNotCreate(dir:String):Unit={
		val directory = new File(dir)
		if(!directory.exists())
			directory.mkdir()
	}
}