package misc

class Path(val src: Int, val dest: Int, val edges: Array[Int], var count:Int, val id:Int) {

	override def equals(that: Any): Boolean =
    that match {
      case that: Path => edges.mkString(" ")==that.edges.mkString(" ")
      case _ => false
   }
   def increment():Unit={
   		count+=1
   }	
}