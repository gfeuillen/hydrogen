package performances

import utils._
import oscar.cp.network.graphLib.topology.DGraph
// import misc.Performance
import java.io.File
import java.io.PrintWriter

object perf_writer{
	def aggregate(graph:DGraph, graphDir:String, test_nums:Seq[Int], outputDir:String):Unit={

		val in_indices = Array[Double](0.0, 0.4, 0.8)
		val out_indices = Array[Double](0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9)
		//val indicess = Array[Double](0.0)
		val realTraceRoutes = s"$graphDir/real_traces"
		val (real_path_matrix,real_path_number) = DataUtils.loadPathMatrix(realTraceRoutes,graph)

		val precisions = Array.fill[Double](10,10)(0.0)
		val recalls = Array.fill[Double](10,10)(0.0)
		val f1Scores = Array.fill[Double](10,10)(0.0)
		val pred_path_number = Array.fill[Double](10,10)(0.0)
		val ins = Array.fill[Double](10,10)(0.0)
		val notIns = Array.fill[Double](10,10)(0.0)
		val shouldBeIns = Array.fill[Double](10,10)(0.0)
		val fulls = Array.fill[Double](10,10)(0.0)
		val partials = Array.fill[Double](10,10)(0.0)
		val nones = Array.fill[Double](10,10)(0.0)

		//Aggregate information
		for(i <- test_nums){
			println(i)
			val test_dir = s"$graphDir/$i"
			for(j <- in_indices; k <- out_indices){
				println(s"Inside $j $k")
				val row = math.round(j*10).toInt
				val col = math.round(k*10).toInt

				val pred_traces_fileName = s"$test_dir/in_$j"+"_"+s"out_$k/out_traces"
				val (pred_path_matrix,_) = DataUtils.loadPathMatrix(pred_traces_fileName,graph)

				val (realEcmpCount,predEcmpCount,in,notIn,shouldBeIn) = Performance.ecmpsCount(real_path_matrix, pred_path_matrix)
				val (precision, recall) = Performance.precisionRecall(real_path_matrix, pred_path_matrix)
				//println(s"$precision $recall")
				val f1Score = Performance.fBeta(precision,recall,1)

				val (full, partial, none) = Performance.fractionFullPartialNone(real_path_matrix,pred_path_matrix)

				precisions(row)(col) += precision
				recalls(row)(col) += recall
				f1Scores(row)(col) += f1Score
				pred_path_number(row)(col) += predEcmpCount
				ins(row)(col) += in
				notIns(row)(col) += notIn
				shouldBeIns(row)(col) += shouldBeIn
				fulls(row)(col) += full
				partials(row)(col) += partial
				nones(row)(col) += none
			}
		}

		for(j <- in_indices; k <- out_indices){
			val row = math.round(j*10).toInt
			val col = math.round(k*10).toInt

			//Compute the mean
			precisions(row)(col) = precisions(row)(col)/test_nums.size
			recalls(row)(col) = recalls(row)(col)/test_nums.size
			f1Scores(row)(col) = f1Scores(row)(col)/test_nums.size
			pred_path_number(row)(col) = pred_path_number(row)(col)/test_nums.size
			ins(row)(col) = ins(row)(col)/test_nums.size
			notIns(row)(col) = notIns(row)(col)/test_nums.size
			shouldBeIns(row)(col) = shouldBeIns(row)(col)/test_nums.size
			fulls(row)(col) = fulls(row)(col)/test_nums.size
			partials(row)(col) = partials(row)(col)/test_nums.size
			nones(row)(col) = nones(row)(col)/test_nums.size
		}

		//write real path number
		var output_writer = new PrintWriter(new File(s"$outputDir/real_path_number"))
		output_writer.write(s"$real_path_number \n")
		output_writer.close()

		//Write precisions
		output_writer = new PrintWriter(new File(s"$outputDir/precisions"))
		output_writer.write(precisions.map(row => row.mkString("---")).mkString("\n"))
		output_writer.close()

		output_writer = new PrintWriter(new File(s"$outputDir/recalls"))
		output_writer.write(recalls.map(row => row.mkString("---")).mkString("\n"))
		output_writer.close()

		output_writer = new PrintWriter(new File(s"$outputDir/f1Scores"))
		output_writer.write(f1Scores.map(row => row.mkString("---")).mkString("\n"))
		output_writer.close()

		output_writer = new PrintWriter(new File(s"$outputDir/pred_path_number"))
		output_writer.write(pred_path_number.map(row => row.mkString("---")).mkString("\n"))
		output_writer.close()

		output_writer = new PrintWriter(new File(s"$outputDir/ins"))
		output_writer.write(ins.map(row => row.mkString("---")).mkString("\n"))
		output_writer.close()

		output_writer = new PrintWriter(new File(s"$outputDir/notIns"))
		output_writer.write(notIns.map(row => row.mkString("---")).mkString("\n"))
		output_writer.close()

		output_writer = new PrintWriter(new File(s"$outputDir/shouldBeIns"))
		output_writer.write(shouldBeIns.map(row => row.mkString("---")).mkString("\n"))
		output_writer.close()

		output_writer = new PrintWriter(new File(s"$outputDir/fulls"))
		output_writer.write(fulls.map(row => row.mkString("---")).mkString("\n"))
		output_writer.close()

		output_writer = new PrintWriter(new File(s"$outputDir/partials"))
		output_writer.write(partials.map(row => row.mkString("---")).mkString("\n"))
		output_writer.close()

		output_writer = new PrintWriter(new File(s"$outputDir/nones"))
		output_writer.write(nones.map(row => row.mkString("---")).mkString("\n"))
		output_writer.close()
	}
}