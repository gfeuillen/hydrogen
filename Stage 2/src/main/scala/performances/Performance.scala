package performances

import oscar.cp.network.graphLib.topology.DGraph
import misc.Path

object Performance{
	def precisionRecall(real:Array[Array[Array[Path]]], pred:Array[Array[Array[Path]]], all:Double):(Double, Double)={
		val n = real.size
		var correct = 0.0
		var total = 0.0
		var incorrect = 0.0
		for(i <- 0 until n; j <- 0 until n; p<-pred(i)(j)){
			total += 1
			if(real(i)(j).contains(p))
				correct += 1
			else{
				incorrect +=1
			}
		}
		(correct/total, correct/all)
	}

	def precisionRecall(real:Array[Array[Array[Path]]], pred:Array[Array[Array[Path]]]):(Double, Double)={
		val n = real.size
		var all = 0.0
		var correct = 0.0
		var total = 0.0
		var incorrect = 0.0
		for(i <- 0 until n; j <- 0 until n){
			for(p<-pred(i)(j)){
				if(real(i)(j).contains(p))
					correct += 1
				else{
					incorrect +=1
				}
			}
			total += pred(i)(j).size
			all += real(i)(j).size
		}
		(correct/total, correct/all)
	}

	def fBeta(precision:Double, recall:Double, beta:Int): Double={
		(1+math.pow(beta,2))*precision*recall/(math.pow(beta,2)*precision+recall)
	}
	def ecmpsCount(real:Array[Array[Array[Path]]], pred:Array[Array[Array[Path]]]):(Double, Double,Double, Double, Double)={
		val n = real.size
		var realEcmpCount = 0.0
		var predEcmpCount = 0.0
		var notIn = 0.0
		var in = 0.0
		var shouldBeIn = 0.0
		for(i <- 0 until n; j <- 0 until n){
			realEcmpCount += real(i)(j).size
			predEcmpCount += pred(i)(j).size
			var subTotal = 0.0
			for(p <- real(i)(j)){
				if(pred(i)(j).contains(p))
					subTotal += 1.0
				else{
					shouldBeIn += 1.0
				}
			}
			in += subTotal
			notIn += (pred(i)(j).size - subTotal)
		}
		(realEcmpCount,predEcmpCount,in,notIn,shouldBeIn)
	}

	def fractionFullPartialNone(real:Array[Array[Array[Path]]], pred:Array[Array[Array[Path]]]):(Double,Double,Double)={
		val n = real.size

		var full = 0.0
		var partial = 0.0
		var none = 0.0

		for(i <- 0 until n; j <- 0 until n){
			var seen = 0.0
			for(p <- pred(i)(j)){
				if(real(i)(j).contains(p))
					seen += 1.0
			}
			if(seen == pred(i)(j).size && seen != 0)
				full += 1.0
			else{
				if (seen == 0)
					none += 1.0
				else
					partial += 1.0
			}
		}
		val tot = n*n
		(full/tot,partial/tot,none/tot)
	}
}