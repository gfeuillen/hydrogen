package utils

import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.Stack
import oscar.cp.network.graphLib.topology._
import misc.Path

object GraphAnalyser{
	def shortestPaths(graph:DGraph, func:Edge => Double): (Array[Array[Array[Path]]],Int)={
		var total = 0
		val pathsMatrix = Array.fill(graph.nNodes, graph.nNodes)(Array[Path]())

		val nodes = 0 until graph.nNodes 
		for(start <- nodes; end <- nodes; path <- shortestPath(graph, func, start, end)){
			total += 1
			pathsMatrix(path.src)(path.dest) = pathsMatrix(path.src)(path.dest) :+ path
		}
		(pathsMatrix, total)
	}
	def shortestPath(graph:DGraph, func:Edge => Double, start:Int, end:Int) : Array[Path] = {

		//INITIALIZATION
		var q = Set[Int]()
		graph.nodes map(n => q = q+n.id)
		
		val previous = Array.fill[ArrayBuffer[Int]](q.size)(ArrayBuffer[Int]())
		val defaultValue = (2<<12).toDouble

		// q map(n => d+= defaultValue)
		var d = Array.fill[Double](q.size)(defaultValue)
		d(start) = 0.0
		while(q.contains(end)){
			var s1 = (q map(n => (d(n), n))).min._2
			q = q - s1
			for (e <- graph.outEdges(s1)){
				val dist = func(e)
				if(d(e.dest.id) > dist+d(s1)){
					d(e.dest.id) = dist+d(s1)
					previous(e.dest.id) = ArrayBuffer[Int](s1)
				}
				//For ECMPS 
				else if (d(e.dest.id) == dist+d(s1))
					previous(e.dest.id) += s1
			}
		}

		val res = ArrayBuffer[Path]()
		
		//INitiliaz the stack avec ce qu'il y a sur le end => plusieurs si plusieurs sur le stack
		val stack = new Stack[(ArrayBuffer[Int], Int)]

		for(state <- previous(end)){
			stack.push((ArrayBuffer[Int](graph.getEdgeId((state, end))), state))
		}

		while(!stack.isEmpty){
			val (path, state) = stack.pop()
			var s = state
			while(s != start){
				for(i <- 1 until previous(s).length){
					val newPath = path.clone() 
					newPath += graph.getEdgeId((previous(s)(i), s))
					stack.push((newPath, previous(s)(i)))
				}
				path += graph.getEdgeId((previous(s)(0), s))
				s = previous(s)(0)
			}
			res += new Path(start,end,path.toArray.reverse,1,-1)
		}
		res.toArray

	}
}