package utils

import scala.io.Source
import java.io.File
import java.io.PrintWriter
import java.nio.file.{Paths, Files}
import oscar.cp.network.graphLib.topology._
import scala.collection.mutable.ArrayBuffer
import misc.Path

object DataPreProcessors {
	def preProcess(subOpt:Boolean, inverse:Boolean)(input:String, output:String):Unit={
		val lines = Source.fromFile(input).getLines
		val out = new File(output)

		if (out.exists())
			out.delete()

		val writer = new PrintWriter(out)


		for(l <- lines){
			val nodes = l.split(" ")
			if (nodes.size >= 2){
				if(subOpt){
					for(len <- 1 until nodes.length-1; index <- 0 until nodes.length-len){
						val tmp = nodes.slice(index,index+len+1)
						writer.write(tmp.mkString(" ")+"\n")
						if(inverse)
							writer.write(tmp.reverse.mkString(" ")+"\n")
					}
				}
				writer.write(nodes.mkString(" ")+"\n")
				if(inverse)
					writer.write(nodes.reverse.mkString(" ")+"\n")
			}
		}
		writer.close()
	}
	
}