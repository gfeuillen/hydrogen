package utils

import oscar.cp.network.graphLib.topology._
import misc.Path

object DataFilters{
	def percentageFilter(perc:Double)(paths:Array[Array[Array[Path]]]):Array[Array[Array[Path]]]={
		val n = paths.size
		val res = Array.fill(n, n)(Array[Path]())
		val r = scala.util.Random
		var rem = 0
		for(i <- 0 until n; j <- 0 until n; p <- paths(i)(j)){
			if(r.nextInt(1000) > (1.0-perc)*1000){
				res(i)(j) = res(i)(j) :+ p
			}else rem += 1
		}
		res
	}
	def inOutFilter(in:Array[String], out:Array[String], couples:Array[(String,String)], graph: => DGraph)(paths:Array[Array[Array[Path]]]):Array[Array[Array[Path]]]={
		val n = paths.size
		val inIDs = in.map(n => graph.getNodeId(n))
		val outIDs = out.map(n => graph.getNodeId(n))
		val coupleIDs = couples map {case (i,o) => (graph.getNodeId(i), graph.getNodeId(o))}

		val res = Array.fill(n, n)(Array[Path]())

		for(i <- 0 until n; j <- 0 until n; p <- paths(i)(j)){
			if (!(inIDs.contains(i) || outIDs.contains(j) || coupleIDs.contains((i,j)))) {
				res(i)(j) = res(i)(j) :+ p
			}
		}
		res
	}
}