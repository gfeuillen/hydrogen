package utils

import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.Map
import scala.io.Source
import oscar.cp.network.graphLib.topology.DGraph
import collection.mutable.HashMap
import misc.Path

object Parser {
	def classicGraphParsing(filePath: String): DGraph = {

		val lines = Source.fromFile(filePath).getLines

		val nodes = ArrayBuffer[(String,Float,Float)]()
		val edges = ArrayBuffer[(Int, Int, Double, Int, Int)]() 

		// Used to recover the id of a node
		val nodeToId: Map[String, Int] = Map()
		val edgeToId: Map[(Int, Int), Int] = Map()

		var switched = false

		for (line <- lines if !line.contains("NODES") && !line.contains("EDGES") && !line.contains("label") && !line.contains("###")) {
			if(line.isEmpty)
				switched = true
			if(!switched){
				//It is a node
				val data = line.split(" ")
				nodeToId(data(0)) = nodes.size
				nodes.append((data(0), data(1).toFloat, data(2).toFloat))
			}
			else{
				//It is an edge
				val data = line.split(" ")
				if(data.length > 2){
					edgeToId((data(1).toInt, data(2).toInt)) = edges.size
					edges.append((data(1).toInt, data(2).toInt,data(3).toDouble, data(4).toInt,data(5).toInt))
				}
			}
		}
		val topology = new DGraph(nodes, edges, nodeToId, edgeToId)
		topology
	}

	def firstStageParsing(filePath: String): DGraph = {
		// println("COUCOU")
		val lines = Source.fromFile(filePath).getLines

		val nodes = ArrayBuffer[(String,Float,Float)]()
		val edges = ArrayBuffer[(Int, Int, Double, Int, Int)]()

		// Used to recover the id of a node
		val nodeToId: Map[String, Int] = Map()
		val edgeToId: Map[(Int, Int), Int] = Map()

		for (line <- lines) {
			//Don't use commented lines
			if(!line.head.equals('#')){
				val data = line.split(" ")
				if (data.size >= 2){
					var start = data(0)
					var end = data(1)
					//val weight = data(2).toFloat

					if(!nodeToId.contains(start)){
						nodeToId(start) = nodeToId.size
						nodes.append((start,0.0f,0.0f))
					}
					var startID = nodeToId(start)

					if(!nodeToId.contains(end)){
						nodeToId(end) = nodeToId.size
						nodes.append((end,0.0f,0.0f))
					}
					var endID = nodeToId(end)

					if(!edgeToId.contains((startID, endID))){
						edgeToId((startID, endID)) = edges.size
						edges.append((startID, endID, 0.0, 0, 0))
					}

					start = data(1)
					end = data(0)
					//val weight = data(2).toFloat

					if(!nodeToId.contains(start)){
						nodeToId(start) = nodeToId.size
						nodes.append((start,0.0f,0.0f))
					}
					startID = nodeToId(start)

					if(!nodeToId.contains(end)){
						nodeToId(end) = nodeToId.size
						nodes.append((end,0.0f,0.0f))
					}
					endID = nodeToId(end)

					if(!edgeToId.contains((startID, endID))){
						edgeToId((startID, endID)) = edges.size
						edges.append((startID, endID, 0.0, 0, 0))
					}
				}
			}
		}
		val topology = new DGraph(nodes, edges, nodeToId, edgeToId)
		topology
	}

	def onlyLinkGraphParsing(filePath: String): DGraph = {
		// println("COUCOU")
		val lines = Source.fromFile(filePath).getLines

		val nodes = ArrayBuffer[(String,Float,Float)]()
		val edges = ArrayBuffer[(Int, Int, Double, Int, Int)]()

		// Used to recover the id of a node
		val nodeToId: Map[String, Int] = Map()
		val edgeToId: Map[(Int, Int), Int] = Map()

		for (line <- lines) {
			//Don't use commented lines
			if(!line.head.equals('#')){
				val data = line.split(" ")
				if (data.size >= 3){
					val start = data(0)
					val end = data(1)
					val weight = data(2).toFloat

					if(!nodeToId.contains(start)){
						nodeToId(start) = nodeToId.size
						nodes.append((start,0.0f,0.0f))
					}
					val startID = nodeToId(start)

					if(!nodeToId.contains(end)){
						nodeToId(end) = nodeToId.size
						nodes.append((end,0.0f,0.0f))
					}
					val endID = nodeToId(end)
					edgeToId((startID, endID)) = edges.size
					edges.append((startID, endID, weight, 0, 0))
				}
			}
		}
		val topology = new DGraph(nodes, edges, nodeToId, edgeToId)
		topology

	}
	def parseTraces(filePath: String, graph:DGraph): (Array[Path], Int) = {

		val lines = Source.fromFile(filePath).getLines

		val paths = ArrayBuffer[Path]()

		var paths2 = HashMap[String, Path]()

		var pathNumber = 0

		for (line <- lines if !line.isEmpty) {
			val data = line.split(" ")
			if (data.length >= 2) {
				val src = graph.getNodeId(data(0))
				val dest = graph.getNodeId(data.last)
				val path = ArrayBuffer[Int]()
				for (i <- 0 until data.length - 1) {
					val u = graph.getNodeId(data(i))
					val v = graph.getNodeId(data(i + 1))

					val tmp1 = data(i)
					val tmp2 = data(i + 1)
					// println(s"Adding $tmp1 $tmp2")
					val edge = (u, v)
					val edgeId = graph.getEdgeId(edge)
					path.append(edgeId)
				}
				val tmp = new Path(src, dest, path.toArray, 1, pathNumber)
				if(paths2 contains tmp.edges.mkString(" ")){
					val p = paths2 get tmp.edges.mkString(" ")
					p.get.increment
				}
				else{
					paths2.put(tmp.edges.mkString(" "),tmp)
					// paths.append(tmp)
					pathNumber += 1
				}
			}
		}

		
		val deterministic = false
		if(deterministic){
			val hasmapkeys = paths2.keys.toArray.sorted
			val arrayPaths = hasmapkeys.map(k => (paths2 get k) get)
			(arrayPaths.reverse, pathNumber)
		}
		else{
			var arrayPaths = paths2.values.toList
			// arrayPaths = scala.util.Random.shuffle(arrayPaths)
			(arrayPaths.toArray, pathNumber)
		}
		
	}
}