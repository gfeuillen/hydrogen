package oscar.cp.network


import gurobi.GRBModel
import gurobi.GRBVar
import gurobi.GRB
import gurobi.GRBQuadExpr
import gurobi.GRBLinExpr
import gurobi.GRBEnv

package object gurobiLib {

  final val M = 1 << 20
  
  final def newModel(): GRBModel = new GRBModel(new GRBEnv())
  
  final def newModel(env: GRBEnv): GRBModel = new GRBModel(env)

  final def floatVar(lb: Double, ub: Double, name: String)(implicit model: GRBModel): GRBVar = {
    model.addVar(lb, ub, 0.0, GRB.CONTINUOUS, name)
  }
  
  final def booleanVar(name: String)(implicit model: GRBModel): GRBVar = {
    model.addVar(0, 1, 0.0, GRB.BINARY, name)
  }
  
  final def integerVar(lb: Int, ub: Int, name: String)(implicit model: GRBModel): GRBVar = {
    model.addVar(lb, ub, 0.0, GRB.INTEGER, name)
  }

  final def maximize(expr: GRBLinExpr)(implicit model: GRBModel): Unit = {
    model.setObjective(expr, GRB.MAXIMIZE)
    model.optimize()
  }

  final def minimize(expr: GRBLinExpr)(implicit model: GRBModel): Unit = {
    model.setObjective(expr, GRB.MINIMIZE)
    model.optimize()
  }

  final def solve()(implicit model: GRBModel): Unit = {
    model.optimize()
  }

  final def sum[@specialized T](iter: TraversableOnce[T])(f: T => Term): GRBLinExpr = {
    val expr = new GRBLinExpr()
    iter.foreach(elem => {
      val term = f(elem)
      expr.addTerm(term.coef, term.variable)
    })
    expr
  }

  final def sum[@specialized T](iter: TraversableOnce[T], filter: T => Boolean)(f: T => Term): GRBLinExpr = {
    val expr = new GRBLinExpr()
    iter.foreach(elem => if (filter(elem)) {
      val term = f(elem)
      expr.addTerm(term.coef, term.variable)
    })
    expr
  }

  final def sum[@specialized T1, @specialized T2](iter1: TraversableOnce[T1], iter2: TraversableOnce[T2])(f: (T1, T2) => Term): GRBLinExpr = {
    val expr = new GRBLinExpr()
    iter1.foreach(elem1 => iter2.foreach(elem2 => {
      val term = f(elem1, elem2)
      expr.addTerm(term.coef, term.variable)
    }))
    expr
  }
  
  final def sum[@specialized T1, @specialized T2](iter1: TraversableOnce[T1], iter2: TraversableOnce[T2], filter: (T1, T2) => Boolean)(f: (T1, T2) => Term): GRBLinExpr = {
    val expr = new GRBLinExpr()
    iter1.foreach(elem1 => iter2.foreach(elem2 => if (filter(elem1, elem2)) {
      val term = f(elem1, elem2)
      expr.addTerm(term.coef, term.variable)
    }))
    expr
  }
  
  implicit final def Var2Term(variable: GRBVar): Term = new Term(1, variable)

  final class Term(val coef: Double, val variable: GRBVar)

  implicit final class IntWrapper(val coef: Int) extends AnyVal {
    final def *(variable: GRBVar): Term = new Term(coef, variable)
  }

  implicit final class VarWrapper(val v: GRBVar) extends AnyVal {

    final def value: Double = v.get(GRB.DoubleAttr.X)

    final def name: String = v.get(GRB.StringAttr.VarName)

    final def *(coef: Int): Term = new Term(coef, v)

    final def *(constant: Double):GRBLinExpr={
      val expr = new GRBLinExpr()
      expr.addTerm(constant,v)
      expr
    }

    final def *(variable: GRBVar):GRBQuadExpr={
      // println("MULTIPLYING : "+v.get(GRB.StringAttr.VarName)+" and "+variable.get(GRB.StringAttr.VarName))
      // println(v.get(GRB.StringAttr.VarName))
      // println(variable.get(GRB.StringAttr.VarName))
      val expr = new GRBQuadExpr()
      expr.addTerm(1.0,v,variable)
      expr
    }

    final def *(linExpr: GRBLinExpr):GRBQuadExpr={
      val expr = new GRBQuadExpr()
      val size = linExpr.size
      for (i <- 0 until size)
        expr.addTerm(linExpr.getCoeff(i), v, linExpr.getVar(i))
      expr
    }

    final def +(variable: GRBVar): GRBLinExpr = {
      val expr = new GRBLinExpr()
      expr.addTerm(1, v)
      expr.addTerm(1, variable)
      expr
    }

    final def -(variable: GRBVar): GRBLinExpr = {
      val expr = new GRBLinExpr()
      expr.addTerm(1, v)
      expr.addTerm(-1, variable)
      expr
    }

    final def -(constant: Double): GRBLinExpr = {
      val expr = new GRBLinExpr()
      expr.addTerm(1, v)
      expr.addConstant(-constant)
      expr
    }

    final def +(linExpr: GRBLinExpr): GRBLinExpr = {
      val expr = new GRBLinExpr()
      expr.addTerm(1, v)
      expr.add(linExpr)
      expr
    }

    final def +(constant: Double): GRBLinExpr = {
      val expr = new GRBLinExpr()
      expr.addTerm(1, v)
      expr.addConstant(constant)
      expr
    }

    final def +(term: Term): GRBLinExpr = {
      val expr = new GRBLinExpr()
      expr.addTerm(1, v)
      expr.addTerm(term.coef, term.variable)
      expr
    }

    final def <=(variable: GRBVar)(implicit model: GRBModel): Unit = {
      model.addConstr(v, GRB.LESS_EQUAL, variable, s"$v <= $variable")
    }

    final def >=(variable: GRBVar)(implicit model: GRBModel): Unit = {
      model.addConstr(v, GRB.GREATER_EQUAL, variable, s"$v >= $variable")
    }

    final def ===(variable: GRBVar)(implicit model: GRBModel): Unit = {
      model.addConstr(v, GRB.EQUAL, variable, s"$v == $variable")
    }

    final def <=(expr: GRBLinExpr)(implicit model: GRBModel): Unit = {
      model.addConstr(v, GRB.LESS_EQUAL, expr, s"$v <= $expr")
    }

    final def <=(constant:Double)(implicit model: GRBModel): Unit = {
      model.addConstr(v, GRB.LESS_EQUAL, constant, s"$v <= $constant")
    }

    final def >=(expr: GRBLinExpr)(implicit model: GRBModel): Unit = {
      model.addConstr(v, GRB.GREATER_EQUAL, expr, s"$v >= $expr")
    }

    final def ===(expr: GRBLinExpr)(implicit model: GRBModel): Unit = {
      model.addConstr(v, GRB.EQUAL, expr, s"$v == $expr")
    }

    final def ===(constant:Double)(implicit model: GRBModel):Unit={
      model.addConstr(v, GRB.EQUAL, constant, s"$v == $constant")
    }
  }

  implicit final class LinWrapper(val lin: GRBLinExpr) extends AnyVal {

    // final def *(constant:Double):GRBLinExpr = {
    //   val expr = new GRBLinExpr()
    //   expr.multAdd(constant,lin)
    //   expr
    // }

    final def opposed:GRBLinExpr = {
        val expr = new GRBLinExpr()
        expr.multAdd(-1,lin)
        expr
    }

    final def -(variable: GRBVar): GRBLinExpr = {
      val expr = new GRBLinExpr()
      expr.add(lin)
      expr.addTerm(-1,variable)
      expr
    }

    final def -(linExpr: GRBLinExpr): GRBLinExpr = {
      val expr = new GRBLinExpr()
      expr.add(lin)
      expr.multAdd(-1,linExpr)
      expr
    }

    final def +(variable: GRBVar): GRBLinExpr = {
      val expr = new GRBLinExpr()
      expr.add(lin)
      expr.addTerm(1, variable)
      expr
    }

    final def +(linExpr: GRBLinExpr): GRBLinExpr = {
      val expr = new GRBLinExpr()
      expr.add(lin)
      expr.add(linExpr)
      expr
    }

    final def +(term: Term): GRBLinExpr = {
      val expr = new GRBLinExpr()
      expr.add(lin)
      expr.addTerm(term.coef, term.variable)
      expr
    }

    final def +=(variable: GRBVar): GRBLinExpr = {
      lin.addTerm(1, variable)
      lin
    }

    final def +=(expr: GRBLinExpr): GRBLinExpr = {
      lin.add(expr)
      lin
    }

    final def +=(term: Term): GRBLinExpr = {
      lin.addTerm(term.coef, term.variable)
      lin
    }

    final def -=(variable: GRBVar): GRBLinExpr = {
      lin.addTerm(-1, variable)
      lin
    }

    final def -=(term: Term): GRBLinExpr = {
      lin.addTerm(-term.coef, term.variable)
      lin
    }

    final def <=(variable: GRBVar)(implicit model: GRBModel): Unit = {
      model.addConstr(lin, GRB.LESS_EQUAL, variable, s"$lin <= $variable")
    }

    final def >=(variable: GRBVar)(implicit model: GRBModel): Unit = {
      model.addConstr(lin, GRB.GREATER_EQUAL, variable, s"$lin >= $variable")
    }

    final def ===(variable: GRBVar)(implicit model: GRBModel): Unit = {
      model.addConstr(lin, GRB.EQUAL, variable, s"$lin == $variable")
    }

    final def <=(expr: GRBLinExpr)(implicit model: GRBModel): Unit = {
      model.addConstr(lin, GRB.LESS_EQUAL, expr, s"$lin <= $expr")
    }

    final def >=(expr: GRBLinExpr)(implicit model: GRBModel): Unit = {
      model.addConstr(lin, GRB.GREATER_EQUAL, expr, s"$lin >= $expr")
    }

    final def ===(expr: GRBLinExpr)(implicit model: GRBModel): Unit = {
      model.addConstr(lin, GRB.EQUAL, expr, s"$lin == $expr")
    }

    final def <=(constant:Double)(implicit model: GRBModel): Unit = {
      model.addConstr(lin, GRB.LESS_EQUAL, constant, s"$lin <= $constant")
    }

    final def >=(constant:Double)(implicit model: GRBModel): Unit = {
      model.addConstr(lin, GRB.GREATER_EQUAL, constant, s"$lin <= $constant")
    }

    final def ===(constant:Double)(implicit model: GRBModel):Unit={
      model.addConstr(lin, GRB.EQUAL, constant, s"$lin == $constant")
    }
  }
  implicit final class QuadWrapper(val v: GRBQuadExpr) extends AnyVal {

    final def +(quadExpr:GRBQuadExpr): GRBQuadExpr={
      val expr = new GRBQuadExpr()
      expr.add(v)
      expr.add(quadExpr)
      expr
    }

    final def +(constant:Double): GRBQuadExpr={
      val expr = new GRBQuadExpr(v)
      expr.addConstant(constant)
      expr
    }

    final def -(quadExpr:GRBQuadExpr): GRBQuadExpr={
      val expr = new GRBQuadExpr()
      expr.add(v)
      expr.multAdd(-1,quadExpr)
      expr
    }

    final def +=(quadExpr:GRBQuadExpr): GRBQuadExpr={
      v.add(quadExpr)
      v
    }

    final def +=(variable:GRBVar): GRBQuadExpr={
      v.addTerm(1.0, variable)
      v
    }

    final def +=(constant:Double): GRBQuadExpr={
      v.addConstant(constant)
      v
    }

    //Assuming GRBQUADEXPR is a single grbquarexpr
    //SHOULD DO a for loop on variables and inverse coefficients NOT USEFUL ANYMORE I THINK
    final def -=(quadExpr:GRBQuadExpr):GRBQuadExpr={
      v.multAdd(-1,quadExpr)
      v
    }

    final def -=(variable:GRBVar):GRBQuadExpr={
      val tmpExpr = new GRBLinExpr()
      tmpExpr -= variable
      v.add(tmpExpr)
      v
    }

    final def ===(constant:Double)(implicit model: GRBModel):Unit={
      model.addQConstr(v, GRB.EQUAL, constant, s"$v == $constant")
    }

    final def ===(variable: GRBVar)(implicit model: GRBModel):Unit={
      model.addQConstr(v, GRB.EQUAL, variable, s"$v == $variable")
    }
  }
}